﻿# About
Synergize is a library made to patch methods easily and intuitively. The purpose of it is akin to [Harmony](https://github.com/pardeike/Harmony), however the design is very different.
