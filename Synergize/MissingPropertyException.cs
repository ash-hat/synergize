using System;

namespace Synergize
{
	/// <inheritdoc />
	/// <summary>
	///     The exception that is thrown when a property cannot be found.
	/// </summary>
	public class MissingPropertyException : MissingMemberException
	{
		public MissingPropertyException(string className, string propertyName)
		{
			ClassName = className;
			MemberName = propertyName;
		}
	}
}
