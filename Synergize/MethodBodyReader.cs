﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Synergize.Instructions;
using Synergize.Instructions.ExceptionBlocks;

namespace Synergize
{
	public class MethodBodyReader : IDisposable
	{
		private static readonly OpCode[] DoubleByteOpcodes;
		private static readonly OpCode[] SingleByteOpcodes;

		static MethodBodyReader()
		{
			SingleByteOpcodes = new[]
			{
				OpCodes.Nop, // 0000
				OpCodes.Break, // 0001
				OpCodes.Ldarg_0, // 0002
				OpCodes.Ldarg_1, // 0003
				OpCodes.Ldarg_2, // 0004
				OpCodes.Ldarg_3, // 0005
				OpCodes.Ldloc_0, // 0006
				OpCodes.Ldloc_1, // 0007
				OpCodes.Ldloc_2, // 0008
				OpCodes.Ldloc_3, // 0009
				OpCodes.Stloc_0, // 000A
				OpCodes.Stloc_1, // 000B
				OpCodes.Stloc_2, // 000C
				OpCodes.Stloc_3, // 000D
				OpCodes.Ldarg_S, // 000E
				OpCodes.Ldarga_S, // 000F
				OpCodes.Starg_S, // 0010
				OpCodes.Ldloc_S, // 0011
				OpCodes.Ldloca_S, // 0012
				OpCodes.Stloc_S, // 0013
				OpCodes.Ldnull, // 0014
				OpCodes.Ldc_I4_M1, // 0015
				OpCodes.Ldc_I4_0, // 0016
				OpCodes.Ldc_I4_1, // 0017
				OpCodes.Ldc_I4_2, // 0018
				OpCodes.Ldc_I4_3, // 0019
				OpCodes.Ldc_I4_4, // 001A
				OpCodes.Ldc_I4_5, // 001B
				OpCodes.Ldc_I4_6, // 001C
				OpCodes.Ldc_I4_7, // 001D
				OpCodes.Ldc_I4_8, // 001E
				OpCodes.Ldc_I4_S, // 001F
				OpCodes.Ldc_I4, // 0020
				OpCodes.Ldc_I8, // 0021
				OpCodes.Ldc_R4, // 0022
				OpCodes.Ldc_R8, // 0023
				OpCodes.Nop, // 0024 (does not exist)
				OpCodes.Dup, // 0025
				OpCodes.Pop, // 0026
				OpCodes.Jmp, // 0027
				OpCodes.Call, // 0028
				OpCodes.Calli, // 0029
				OpCodes.Ret, // 002A
				OpCodes.Br_S, // 002B
				OpCodes.Brfalse_S, // 002C
				OpCodes.Brtrue_S, // 002D
				OpCodes.Beq_S, // 002E
				OpCodes.Bge_S, // 002F
				OpCodes.Bgt_S, // 0030
				OpCodes.Ble_S, // 0031
				OpCodes.Blt_S, // 0032
				OpCodes.Bne_Un_S, // 0033
				OpCodes.Bge_Un_S, // 0034
				OpCodes.Bgt_Un_S, // 0035
				OpCodes.Ble_Un_S, // 0036
				OpCodes.Blt_Un_S, // 0037
				OpCodes.Br, // 0038
				OpCodes.Brfalse, // 0039
				OpCodes.Brtrue, // 003A
				OpCodes.Beq, // 003B
				OpCodes.Bge, // 003C
				OpCodes.Bgt, // 003D
				OpCodes.Ble, // 003E
				OpCodes.Blt, // 003F
				OpCodes.Bne_Un, // 0040
				OpCodes.Bge_Un, // 0041
				OpCodes.Bgt_Un, // 0042
				OpCodes.Ble_Un, // 0043
				OpCodes.Blt_Un, // 0044
				OpCodes.Switch, // 0045
				OpCodes.Ldind_I1, // 0046
				OpCodes.Ldind_U1, // 0047
				OpCodes.Ldind_I2, // 0048
				OpCodes.Ldind_U2, // 0049
				OpCodes.Ldind_I4, // 004A
				OpCodes.Ldind_U4, // 004B
				OpCodes.Ldind_I8, // 004C
				OpCodes.Ldind_I, // 004D
				OpCodes.Ldind_R4, // 004E
				OpCodes.Ldind_R8, // 004F
				OpCodes.Ldind_Ref, // 0050
				OpCodes.Stind_Ref, // 0051
				OpCodes.Stind_I1, // 0052
				OpCodes.Stind_I2, // 0053
				OpCodes.Stind_I4, // 0054
				OpCodes.Stind_I8, // 0055
				OpCodes.Stind_R4, // 0056
				OpCodes.Stind_R8, // 0057
				OpCodes.Add, // 0058
				OpCodes.Sub, // 0059
				OpCodes.Mul, // 005A
				OpCodes.Div, // 005B
				OpCodes.Div_Un, // 005C
				OpCodes.Rem, // 005D
				OpCodes.Rem_Un, // 005E
				OpCodes.And, // 005F
				OpCodes.Or, // 0060
				OpCodes.Xor, // 0061
				OpCodes.Shl, // 0062
				OpCodes.Shr, // 0063
				OpCodes.Shr_Un, // 0064
				OpCodes.Neg, // 0065
				OpCodes.Not, // 0066
				OpCodes.Conv_I1, // 0067
				OpCodes.Conv_I2, // 0068
				OpCodes.Conv_I4, // 0069
				OpCodes.Conv_I8, // 006A
				OpCodes.Conv_R4, // 006B
				OpCodes.Conv_R8, // 006C
				OpCodes.Conv_U4, // 006D
				OpCodes.Conv_U8, // 006E
				OpCodes.Callvirt, // 006F
				OpCodes.Cpobj, // 0070
				OpCodes.Ldobj, // 0071
				OpCodes.Ldstr, // 0072
				OpCodes.Newobj, // 0073
				OpCodes.Castclass, // 0074
				OpCodes.Isinst, // 0075
				OpCodes.Conv_R_Un, // 0076
				OpCodes.Nop, // 0077 (does not exist)
				OpCodes.Nop, // 0078 (does not exist)
				OpCodes.Unbox, // 0079
				OpCodes.Throw, // 007A
				OpCodes.Ldfld, // 007B
				OpCodes.Ldflda, // 007C
				OpCodes.Stfld, // 007D
				OpCodes.Ldsfld, // 007E
				OpCodes.Ldsflda, // 007F
				OpCodes.Stsfld, // 0080
				OpCodes.Stobj, // 0081
				OpCodes.Conv_Ovf_I1_Un, // 0082
				OpCodes.Conv_Ovf_I2_Un, // 0083
				OpCodes.Conv_Ovf_I4_Un, // 0084
				OpCodes.Conv_Ovf_I8_Un, // 0085
				OpCodes.Conv_Ovf_U1_Un, // 0086
				OpCodes.Conv_Ovf_U2_Un, // 0087
				OpCodes.Conv_Ovf_U4_Un, // 0088
				OpCodes.Conv_Ovf_U8_Un, // 0089
				OpCodes.Conv_Ovf_I_Un, // 008A
				OpCodes.Conv_Ovf_U_Un, // 008B
				OpCodes.Box, // 008C
				OpCodes.Newarr, // 008D
				OpCodes.Ldlen, // 008E
				OpCodes.Ldelema, // 008F
				OpCodes.Ldelem_I1, // 0090
				OpCodes.Ldelem_U1, // 0091
				OpCodes.Ldelem_I2, // 0092
				OpCodes.Ldelem_U2, // 0093
				OpCodes.Ldelem_I4, // 0094
				OpCodes.Ldelem_U4, // 0095
				OpCodes.Ldelem_I8, // 0096
				OpCodes.Ldelem_I, // 0097
				OpCodes.Ldelem_R4, // 0098
				OpCodes.Ldelem_R8, // 0099
				OpCodes.Ldelem_Ref, // 009A
				OpCodes.Stelem_I, // 009B
				OpCodes.Stelem_I1, // 009C
				OpCodes.Stelem_I2, // 009D
				OpCodes.Stelem_I4, // 009E
				OpCodes.Stelem_I8, // 009F
				OpCodes.Stelem_R4, // 00A0
				OpCodes.Stelem_R8, // 00A1
				OpCodes.Stelem_Ref, // 00A2
				OpCodes.Ldelem, // 00A3
				OpCodes.Stelem, // 00A4
				OpCodes.Unbox_Any, // 00A5
				OpCodes.Nop, // 00A6 (does not exist)
				OpCodes.Nop, // 00A7 (does not exist)
				OpCodes.Nop, // 00A8 (does not exist)
				OpCodes.Nop, // 00A9 (does not exist)
				OpCodes.Nop, // 00AA (does not exist)
				OpCodes.Nop, // 00AB (does not exist)
				OpCodes.Nop, // 00AC (does not exist)
				OpCodes.Nop, // 00AD (does not exist)
				OpCodes.Nop, // 00AE (does not exist)
				OpCodes.Nop, // 00AF (does not exist)
				OpCodes.Nop, // 00B0 (does not exist)
				OpCodes.Nop, // 00B1 (does not exist)
				OpCodes.Nop, // 00B2 (does not exist)
				OpCodes.Conv_Ovf_I1, // 00B3
				OpCodes.Conv_Ovf_U1, // 00B4
				OpCodes.Conv_Ovf_I2, // 00B5
				OpCodes.Conv_Ovf_U2, // 00B6
				OpCodes.Conv_Ovf_I4, // 00B7
				OpCodes.Conv_Ovf_U4, // 00B8
				OpCodes.Conv_Ovf_I8, // 00B9
				OpCodes.Conv_Ovf_U8, // 00BA
				OpCodes.Nop, // 00BB (does not exist)
				OpCodes.Nop, // 00BC (does not exist)
				OpCodes.Nop, // 00BD (does not exist)
				OpCodes.Nop, // 00BE (does not exist)
				OpCodes.Nop, // 00BF (does not exist)
				OpCodes.Nop, // 00C0 (does not exist)
				OpCodes.Nop, // 00C1 (does not exist)
				OpCodes.Refanyval, // 00C2
				OpCodes.Ckfinite, // 00C3
				OpCodes.Nop, // 00C4 (does not exist)
				OpCodes.Nop, // 00C5 (does not exist)
				OpCodes.Mkrefany, // 00C6
				OpCodes.Nop, // 00C7 (does not exist)
				OpCodes.Nop, // 00C8 (does not exist)
				OpCodes.Nop, // 00C9 (does not exist)
				OpCodes.Nop, // 00CA (does not exist)
				OpCodes.Nop, // 00CB (does not exist)
				OpCodes.Nop, // 00CC (does not exist)
				OpCodes.Nop, // 00CD (does not exist)
				OpCodes.Nop, // 00CE (does not exist)
				OpCodes.Nop, // 00CF (does not exist)
				OpCodes.Ldtoken, // 00D0
				OpCodes.Conv_U2, // 00D1
				OpCodes.Conv_U1, // 00D2
				OpCodes.Conv_I, // 00D3
				OpCodes.Conv_Ovf_I, // 00D4
				OpCodes.Conv_Ovf_U, // 00D5
				OpCodes.Add_Ovf, // 00D6
				OpCodes.Add_Ovf_Un, // 00D7
				OpCodes.Mul_Ovf, // 00D8
				OpCodes.Mul_Ovf_Un, // 00D9
				OpCodes.Sub_Ovf, // 00DA
				OpCodes.Sub_Ovf_Un, // 00DB
				OpCodes.Endfinally, // 00DC
				OpCodes.Leave, // 00DD
				OpCodes.Leave_S, // 00DE
				OpCodes.Stind_I, // 00DF
				OpCodes.Conv_U, // 00E0
				OpCodes.Nop, // 00E1
				OpCodes.Nop, // 00E2
				OpCodes.Nop, // 00E3
				OpCodes.Nop, // 00E4
				OpCodes.Nop, // 00E5
				OpCodes.Nop, // 00E6
				OpCodes.Nop, // 00E7
				OpCodes.Nop, // 00E8
				OpCodes.Nop, // 00E9
				OpCodes.Nop, // 00EA
				OpCodes.Nop, // 00EB
				OpCodes.Nop, // 00EC
				OpCodes.Nop, // 00ED
				OpCodes.Nop, // 00EE
				OpCodes.Nop, // 00EF
				OpCodes.Nop, // 00F0
				OpCodes.Nop, // 00F1
				OpCodes.Nop, // 00F2
				OpCodes.Nop, // 00F3
				OpCodes.Nop, // 00F4
				OpCodes.Nop, // 00F5
				OpCodes.Nop, // 00F6
				OpCodes.Nop, // 00F7
				OpCodes.Prefix7, // 00F8
				OpCodes.Prefix6, // 00F9
				OpCodes.Prefix5, // 00FA
				OpCodes.Prefix4, // 00FB
				OpCodes.Prefix3, // 00FC
				OpCodes.Prefix2, // 00FD
				OpCodes.Prefix1, // 00FE
				OpCodes.Prefixref // 00FF
			};

			DoubleByteOpcodes = new[]
			{
				OpCodes.Arglist, // FE00
				OpCodes.Ceq, // FE01
				OpCodes.Cgt, // FE02
				OpCodes.Cgt_Un, // FE03
				OpCodes.Clt, // FE04
				OpCodes.Clt_Un, // FE05
				OpCodes.Ldftn, // FE06
				OpCodes.Ldvirtftn, // FE07
				OpCodes.Nop, // FE08 (does not exist)
				OpCodes.Ldarg, // FE09
				OpCodes.Ldarga, // FE0A
				OpCodes.Starg, // FE0B
				OpCodes.Ldloc, // FE0C
				OpCodes.Ldloca, // FE0D
				OpCodes.Stloc, // FE0E
				OpCodes.Localloc, // FE0F
				OpCodes.Nop, // FE10 (does not exist)
				OpCodes.Endfilter, // FE11
				OpCodes.Unaligned, // FE12
				OpCodes.Volatile, // FE13
				OpCodes.Tailcall, // FE14
				OpCodes.Initobj, // FE15
				OpCodes.Constrained, // FE16
				OpCodes.Cpblk, // FE17
				OpCodes.Initblk, // FE18
				OpCodes.Nop, // FE19 (does not exist)
				OpCodes.Rethrow, // FE1A
				OpCodes.Nop, // FE1B (does not exist)
				OpCodes.Sizeof, // FE1C
				OpCodes.Refanytype, // FE1D
				OpCodes.Readonly, // FE1E
				OpCodes.Nop, // FE1F (does not exist)
				OpCodes.Nop, // FE20 (does not exist)
				OpCodes.Nop, // FE21 (does not exist)
				OpCodes.Nop, // FE22 (does not exist)
				OpCodes.Nop, // FE23 (does not exist)
				OpCodes.Nop, // FE24 (does not exist)
				OpCodes.Nop, // FE25 (does not exist)
				OpCodes.Nop, // FE26 (does not exist)
				OpCodes.Nop, // FE27 (does not exist)
				OpCodes.Nop, // FE28 (does not exist)
				OpCodes.Nop, // FE29 (does not exist)
				OpCodes.Nop, // FE2A (does not exist)
				OpCodes.Nop, // FE2B (does not exist)
				OpCodes.Nop, // FE2C (does not exist)
				OpCodes.Nop, // FE2D (does not exist)
				OpCodes.Nop, // FE2E (does not exist)
				OpCodes.Nop, // FE2F (does not exist)
				OpCodes.Nop, // FE30 (does not exist)
				OpCodes.Nop, // FE31 (does not exist)
				OpCodes.Nop, // FE32 (does not exist)
				OpCodes.Nop, // FE33 (does not exist)
				OpCodes.Nop, // FE34 (does not exist)
				OpCodes.Nop, // FE35 (does not exist)
				OpCodes.Nop, // FE36 (does not exist)
				OpCodes.Nop, // FE37 (does not exist)
				OpCodes.Nop, // FE38 (does not exist)
				OpCodes.Nop, // FE39 (does not exist)
				OpCodes.Nop, // FE3A (does not exist)
				OpCodes.Nop, // FE3B (does not exist)
				OpCodes.Nop, // FE3C (does not exist)
				OpCodes.Nop, // FE3D (does not exist)
				OpCodes.Nop, // FE3E (does not exist)
				OpCodes.Nop, // FE3F (does not exist)
				OpCodes.Nop, // FE40 (does not exist)
				OpCodes.Nop, // FE41 (does not exist)
				OpCodes.Nop, // FE42 (does not exist)
				OpCodes.Nop, // FE43 (does not exist)
				OpCodes.Nop, // FE44 (does not exist)
				OpCodes.Nop, // FE45 (does not exist)
				OpCodes.Nop, // FE46 (does not exist)
				OpCodes.Nop, // FE47 (does not exist)
				OpCodes.Nop, // FE48 (does not exist)
				OpCodes.Nop, // FE49 (does not exist)
				OpCodes.Nop, // FE4A (does not exist)
				OpCodes.Nop, // FE4B (does not exist)
				OpCodes.Nop, // FE4C (does not exist)
				OpCodes.Nop, // FE4D (does not exist)
				OpCodes.Nop, // FE4E (does not exist)
				OpCodes.Nop, // FE4F (does not exist)
				OpCodes.Nop, // FE50 (does not exist)
				OpCodes.Nop, // FE51 (does not exist)
				OpCodes.Nop, // FE52 (does not exist)
				OpCodes.Nop, // FE53 (does not exist)
				OpCodes.Nop, // FE54 (does not exist)
				OpCodes.Nop, // FE55 (does not exist)
				OpCodes.Nop, // FE56 (does not exist)
				OpCodes.Nop, // FE57 (does not exist)
				OpCodes.Nop, // FE58 (does not exist)
				OpCodes.Nop, // FE59 (does not exist)
				OpCodes.Nop, // FE5A (does not exist)
				OpCodes.Nop, // FE5B (does not exist)
				OpCodes.Nop, // FE5C (does not exist)
				OpCodes.Nop, // FE5D (does not exist)
				OpCodes.Nop, // FE5E (does not exist)
				OpCodes.Nop, // FE5F (does not exist)
				OpCodes.Nop, // FE60 (does not exist)
				OpCodes.Nop, // FE61 (does not exist)
				OpCodes.Nop, // FE62 (does not exist)
				OpCodes.Nop, // FE63 (does not exist)
				OpCodes.Nop, // FE64 (does not exist)
				OpCodes.Nop, // FE65 (does not exist)
				OpCodes.Nop, // FE66 (does not exist)
				OpCodes.Nop, // FE67 (does not exist)
				OpCodes.Nop, // FE68 (does not exist)
				OpCodes.Nop, // FE69 (does not exist)
				OpCodes.Nop, // FE6A (does not exist)
				OpCodes.Nop, // FE6B (does not exist)
				OpCodes.Nop, // FE6C (does not exist)
				OpCodes.Nop, // FE6D (does not exist)
				OpCodes.Nop, // FE6E (does not exist)
				OpCodes.Nop, // FE6F (does not exist)
				OpCodes.Nop, // FE70 (does not exist)
				OpCodes.Nop, // FE71 (does not exist)
				OpCodes.Nop, // FE72 (does not exist)
				OpCodes.Nop, // FE73 (does not exist)
				OpCodes.Nop, // FE74 (does not exist)
				OpCodes.Nop, // FE75 (does not exist)
				OpCodes.Nop, // FE76 (does not exist)
				OpCodes.Nop, // FE77 (does not exist)
				OpCodes.Nop, // FE78 (does not exist)
				OpCodes.Nop, // FE79 (does not exist)
				OpCodes.Nop, // FE7A (does not exist)
				OpCodes.Nop, // FE7B (does not exist)
				OpCodes.Nop, // FE7C (does not exist)
				OpCodes.Nop, // FE7D (does not exist)
				OpCodes.Nop, // FE7E (does not exist)
				OpCodes.Nop, // FE7F (does not exist)
				OpCodes.Nop, // FE80 (does not exist)
				OpCodes.Nop, // FE81 (does not exist)
				OpCodes.Nop, // FE82 (does not exist)
				OpCodes.Nop, // FE83 (does not exist)
				OpCodes.Nop, // FE84 (does not exist)
				OpCodes.Nop, // FE85 (does not exist)
				OpCodes.Nop, // FE86 (does not exist)
				OpCodes.Nop, // FE87 (does not exist)
				OpCodes.Nop, // FE88 (does not exist)
				OpCodes.Nop, // FE89 (does not exist)
				OpCodes.Nop, // FE8A (does not exist)
				OpCodes.Nop, // FE8B (does not exist)
				OpCodes.Nop, // FE8C (does not exist)
				OpCodes.Nop, // FE8D (does not exist)
				OpCodes.Nop, // FE8E (does not exist)
				OpCodes.Nop, // FE8F (does not exist)
				OpCodes.Nop, // FE90 (does not exist)
				OpCodes.Nop, // FE91 (does not exist)
				OpCodes.Nop, // FE92 (does not exist)
				OpCodes.Nop, // FE93 (does not exist)
				OpCodes.Nop, // FE94 (does not exist)
				OpCodes.Nop, // FE95 (does not exist)
				OpCodes.Nop, // FE96 (does not exist)
				OpCodes.Nop, // FE97 (does not exist)
				OpCodes.Nop, // FE98 (does not exist)
				OpCodes.Nop, // FE99 (does not exist)
				OpCodes.Nop, // FE9A (does not exist)
				OpCodes.Nop, // FE9B (does not exist)
				OpCodes.Nop, // FE9C (does not exist)
				OpCodes.Nop, // FE9D (does not exist)
				OpCodes.Nop, // FE9E (does not exist)
				OpCodes.Nop, // FE9F (does not exist)
				OpCodes.Nop, // FEA0 (does not exist)
				OpCodes.Nop, // FEA1 (does not exist)
				OpCodes.Nop, // FEA2 (does not exist)
				OpCodes.Nop, // FEA3 (does not exist)
				OpCodes.Nop, // FEA4 (does not exist)
				OpCodes.Nop, // FEA5 (does not exist)
				OpCodes.Nop, // FEA6 (does not exist)
				OpCodes.Nop, // FEA7 (does not exist)
				OpCodes.Nop, // FEA8 (does not exist)
				OpCodes.Nop, // FEA9 (does not exist)
				OpCodes.Nop, // FEAA (does not exist)
				OpCodes.Nop, // FEAB (does not exist)
				OpCodes.Nop, // FEAC (does not exist)
				OpCodes.Nop, // FEAD (does not exist)
				OpCodes.Nop, // FEAE (does not exist)
				OpCodes.Nop, // FEAF (does not exist)
				OpCodes.Nop, // FEB0 (does not exist)
				OpCodes.Nop, // FEB1 (does not exist)
				OpCodes.Nop, // FEB2 (does not exist)
				OpCodes.Nop, // FEB3 (does not exist)
				OpCodes.Nop, // FEB4 (does not exist)
				OpCodes.Nop, // FEB5 (does not exist)
				OpCodes.Nop, // FEB6 (does not exist)
				OpCodes.Nop, // FEB7 (does not exist)
				OpCodes.Nop, // FEB8 (does not exist)
				OpCodes.Nop, // FEB9 (does not exist)
				OpCodes.Nop, // FEBA (does not exist)
				OpCodes.Nop, // FEBB (does not exist)
				OpCodes.Nop, // FEBC (does not exist)
				OpCodes.Nop, // FEBD (does not exist)
				OpCodes.Nop, // FEBE (does not exist)
				OpCodes.Nop, // FEBF (does not exist)
				OpCodes.Nop, // FEC0 (does not exist)
				OpCodes.Nop, // FEC1 (does not exist)
				OpCodes.Nop, // FEC2 (does not exist)
				OpCodes.Nop, // FEC3 (does not exist)
				OpCodes.Nop, // FEC4 (does not exist)
				OpCodes.Nop, // FEC5 (does not exist)
				OpCodes.Nop, // FEC6 (does not exist)
				OpCodes.Nop, // FEC7 (does not exist)
				OpCodes.Nop, // FEC8 (does not exist)
				OpCodes.Nop, // FEC9 (does not exist)
				OpCodes.Nop, // FECA (does not exist)
				OpCodes.Nop, // FECB (does not exist)
				OpCodes.Nop, // FECC (does not exist)
				OpCodes.Nop, // FECD (does not exist)
				OpCodes.Nop, // FECE (does not exist)
				OpCodes.Nop, // FECF (does not exist)
				OpCodes.Nop, // FED0 (does not exist)
				OpCodes.Nop, // FED1 (does not exist)
				OpCodes.Nop, // FED2 (does not exist)
				OpCodes.Nop, // FED3 (does not exist)
				OpCodes.Nop, // FED4 (does not exist)
				OpCodes.Nop, // FED5 (does not exist)
				OpCodes.Nop, // FED6 (does not exist)
				OpCodes.Nop, // FED7 (does not exist)
				OpCodes.Nop, // FED8 (does not exist)
				OpCodes.Nop, // FED9 (does not exist)
				OpCodes.Nop, // FEDA (does not exist)
				OpCodes.Nop, // FEDB (does not exist)
				OpCodes.Nop, // FEDC (does not exist)
				OpCodes.Nop, // FEDD (does not exist)
				OpCodes.Nop, // FEDE (does not exist)
				OpCodes.Nop, // FEDF (does not exist)
				OpCodes.Nop, // FEE0 (does not exist)
				OpCodes.Nop, // FEE1 (does not exist)
				OpCodes.Nop, // FEE2 (does not exist)
				OpCodes.Nop, // FEE3 (does not exist)
				OpCodes.Nop, // FEE4 (does not exist)
				OpCodes.Nop, // FEE5 (does not exist)
				OpCodes.Nop, // FEE6 (does not exist)
				OpCodes.Nop, // FEE7 (does not exist)
				OpCodes.Nop, // FEE8 (does not exist)
				OpCodes.Nop, // FEE9 (does not exist)
				OpCodes.Nop, // FEEA (does not exist)
				OpCodes.Nop, // FEEB (does not exist)
				OpCodes.Nop, // FEEC (does not exist)
				OpCodes.Nop, // FEED (does not exist)
				OpCodes.Nop, // FEEE (does not exist)
				OpCodes.Nop, // FEEF (does not exist)
				OpCodes.Nop, // FEF0 (does not exist)
				OpCodes.Nop, // FEF1 (does not exist)
				OpCodes.Nop, // FEF2 (does not exist)
				OpCodes.Nop, // FEF3 (does not exist)
				OpCodes.Nop, // FEF4 (does not exist)
				OpCodes.Nop, // FEF5 (does not exist)
				OpCodes.Nop, // FEF6 (does not exist)
				OpCodes.Nop, // FEF7 (does not exist)
				OpCodes.Nop, // FEF8 (does not exist)
				OpCodes.Nop, // FEF9 (does not exist)
				OpCodes.Nop, // FEFA (does not exist)
				OpCodes.Nop, // FEFB (does not exist)
				OpCodes.Nop, // FEFC (does not exist)
				OpCodes.Nop, // FEFD (does not exist)
				OpCodes.Nop, // FEFE (does not exist)
				OpCodes.Nop // FEFF (does not exist)
			};

			const int expectedLength = byte.MaxValue + 1;

			if (SingleByteOpcodes.Length != expectedLength)
			{
				throw new InvalidDataException("Invalid amount of single-byte opcodes in the lookup array (expected " + expectedLength + $", got {SingleByteOpcodes.Length}).");
			}

			if (DoubleByteOpcodes.Length != expectedLength)
			{
				throw new InvalidDataException("Invalid amount of double-byte opcodes in the lookup array (expected " + expectedLength + $", got {DoubleByteOpcodes.Length}).");
			}
		}

		private readonly MemoryStream _rawIL;

		private readonly int _rawILLength;
		private readonly BinaryReader _reader;

		public Module ContainingModule { get; }
		public IList<ExceptionHandlingClause> ExceptionHandlers { get; }

		public IReadOnlyList<IEmittableInstruction> Instructions { get; }
		public IList<LocalVariableInfo> LocalVariables { get; }
		public Type[] MethodGenerics { get; }
		public ParameterInfo[] Parameters { get; }
		public Type[] TypeGenerics { get; }

		public MethodBodyReader(MethodBase method)
		{
			MethodBody body = method.GetMethodBody();
			if (body == null)
			{
				throw new ArgumentException("Method returned a null body.", nameof(method));
			}

			byte[] rawILBytes = body.GetILAsByteArray();
			_rawILLength = rawILBytes.Length;
			_rawIL = new MemoryStream(rawILBytes);
			_reader = new BinaryReader(_rawIL);

			ContainingModule = method.Module;

			Type declarer = method.DeclaringType;
			if (declarer == null)
			{
				throw new ArgumentException("Method returned a null declaring type.", nameof(method));
			}

			TypeGenerics =
				declarer.IsGenericType
					? declarer.GetGenericArguments()
					: new Type[0];

			MethodGenerics =
				method.IsGenericMethod
					? method.GetGenericArguments()
					: new Type[0];

			LocalVariables = body.LocalVariables;
			ExceptionHandlers = body.ExceptionHandlingClauses;

			Parameters = method.GetParameters();
			if (!method.IsStatic)
			{
				ParameterInfo[] instanceArguments = new ParameterInfo[Parameters.Length + 1];

				instanceArguments[0] = new ThisParameter(method);
				Array.Copy(Parameters, 0, instanceArguments, 1, Parameters.Length);

				Parameters = instanceArguments;
			}

			List<IInstruction> instructions = ReadInstructionsToEnd().ToList();

			foreach (ExceptionHandlingClause handler in ExceptionHandlers)
			{
				ExceptionBlock startTryBlock = new ExceptionBlock();
				instructions.FindByOffset(handler.TryOffset).ExceptionHandlers.Add(startTryBlock);

				EndHandlerBlock endAllBlocks = new EndHandlerBlock(startTryBlock);
				instructions.FindByOffset(handler.HandlerOffset + handler.HandlerLength).ExceptionHandlers.Add(endAllBlocks);

				ExceptionClauseBlock clause;
				int offset;
				switch (handler.Flags)
				{
					case ExceptionHandlingClauseOptions.Clause:
						offset = handler.HandlerOffset;
						clause = new CatchClauseBlock(startTryBlock, endAllBlocks, handler.CatchType);
						break;

					case ExceptionHandlingClauseOptions.Filter:
						offset = handler.FilterOffset;
						clause = new FilterClauseBlock(startTryBlock, endAllBlocks);
						break;

					case ExceptionHandlingClauseOptions.Finally:
						offset = handler.HandlerOffset;
						clause = new FinallyClauseBlock(startTryBlock, endAllBlocks);
						break;

					case ExceptionHandlingClauseOptions.Fault:
						offset = handler.HandlerOffset;
						clause = new CatchClauseBlock(startTryBlock, endAllBlocks);
						break;

					default: throw new NotSupportedException();
				}

				instructions.FindByOffset(offset).ExceptionHandlers.Add(clause);
			}

			IReadOnlyList<IInstruction> readonlyInstructions = new ReadOnlyCollection<IInstruction>(instructions);

			IEmittableInstruction[] emittables = new IEmittableInstruction[instructions.Count];

			for (int i = 0; i < instructions.Count; ++i)
			{
				IInstruction il = instructions[i];
				IResolvableInstruction resolvableIl = il as IResolvableInstruction;
				if (resolvableIl != null)
				{
					emittables[i] = resolvableIl.Resolve(readonlyInstructions);
				}
				else
				{
					emittables[i] = (IEmittableInstruction) il;
				}
			}

			Instructions = new ReadOnlyCollection<IEmittableInstruction>(emittables);

#if DEBUG
			Console.WriteLine($"Read method {method.ToFullString()}:");
			Console.WriteLine();

			foreach (IEmittableInstruction il in Instructions)
			{
				Console.WriteLine(il);
			}
			
			Console.WriteLine();
#endif
		}

		public void Dispose()
		{
			_reader.Dispose();
			_rawIL.Dispose();
		}

		private OpCode ReadOpCode()
		{
			const string unsupportedOpcodeMessage =
				"Unsupported opcode was read. This likely means there was an error with reading previous instructions (read too much or too little), or the source is corrupt.";

			byte firstByte = _reader.ReadByte();
			OpCode opcode;

			if (firstByte != 0xFE)
			{
				opcode = SingleByteOpcodes[firstByte];

				// Array returns nop if the opcode is not found.
				if (firstByte != 0x00 && opcode == OpCodes.Nop)
				{
					throw new NotSupportedException(unsupportedOpcodeMessage);
				}
			}
			else
			{
				opcode = DoubleByteOpcodes[_reader.ReadByte()];

				if (opcode == OpCodes.Nop)
				{
					throw new NotSupportedException(unsupportedOpcodeMessage);
				}
			}

			return opcode;
		}

		private IInstruction ReadInstruction(int offset)
		{
			OpCode operation = ReadOpCode();

			switch (operation.OperandType)
			{
				case OperandType.InlineNone:
					return new OpCodeInstruction(operation)
					{
						Offset = offset
					};

				case OperandType.InlineSwitch:
				{
					int[] targets = new int[_reader.ReadInt32()];
					for (int i = 0; i < targets.Length; i++)
					{
						targets[i] = _reader.ReadInt32();
					}

					return new ResolvableSwitchInstruction(targets, offset);
				}

				case OperandType.ShortInlineBrTarget: return ResolvableBranchInstruction.FromShort(operation, _reader.ReadSByte(), offset);
				case OperandType.InlineBrTarget: return ResolvableBranchInstruction.FromFull(operation, _reader.ReadInt32(), offset);

				case OperandType.ShortInlineI:
					return new ByteOperandInstruction(operation, _reader.ReadByte())
					{
						Offset = offset
					};

				case OperandType.InlineI:
					return new IntOperandInstruction(operation, _reader.ReadInt32())
					{
						Offset = offset
					};

				case OperandType.InlineI8:
					return new LongOperandInstruction(operation, _reader.ReadInt64())
					{
						Offset = offset
					};

				case OperandType.ShortInlineR:
					return new FloatOperandInstruction(operation, _reader.ReadSingle())
					{
						Offset = offset
					};

				case OperandType.InlineR:
					return new DoubleOperandInstruction(operation, _reader.ReadDouble())
					{
						Offset = offset
					};

				case OperandType.InlineString:
					return new StringOperandInstruction(operation, ContainingModule.ResolveString(_reader.ReadInt32()))
					{
						Offset = offset
					};

				case OperandType.InlineTok:
				{
					MemberInfo member = ContainingModule.ResolveMember(_reader.ReadInt32(), TypeGenerics, MethodGenerics);

					switch (member.MemberType)
					{
						case MemberTypes.Constructor: return new ConstructorOperandInstruction(operation, (ConstructorInfo) member);

						case MemberTypes.Field: return new FieldOperandInstruction(operation, (FieldInfo) member);

						case MemberTypes.Method: return new MethodOperandInstruction(operation, (MethodInfo) member);

						case MemberTypes.TypeInfo:
						case MemberTypes.NestedType:
							return new TypeOperandInstruction(operation, (Type) member);

						default: throw new NotSupportedException();
					}
				}

				case OperandType.InlineType:
					return new TypeOperandInstruction(operation, ContainingModule.ResolveType(_reader.ReadInt32(), TypeGenerics, MethodGenerics))
					{
						Offset = offset
					};

				case OperandType.InlineMethod:
				{
					MethodBase method = ContainingModule.ResolveMethod(_reader.ReadInt32(), TypeGenerics, MethodGenerics);

					return
						method.MemberType == MemberTypes.Constructor
							? (IInstruction) new ConstructorOperandInstruction(operation, (ConstructorInfo) method)
							{
								Offset = offset
							}
							: new MethodOperandInstruction(operation, (MethodInfo) method)
							{
								Offset = offset
							};
				}

				case OperandType.InlineField:
					return new FieldOperandInstruction(operation, ContainingModule.ResolveField(_reader.ReadInt32(), TypeGenerics, MethodGenerics))
					{
						Offset = offset
					};

				case OperandType.ShortInlineVar:
					return new ByteOperandInstruction(operation, _reader.ReadByte())
					{
						Offset = offset
					};

				case OperandType.InlineVar:
					return new UShortOperandInstruction(operation, _reader.ReadUInt16())
					{
						Offset = offset
					};

				default: throw new NotSupportedException();
			}
		}

		private IInstruction ReadInstruction()
		{
			int offset = (int) _rawIL.Position;

			return ReadInstruction(offset);
		}

		private IEnumerable<IInstruction> ReadInstructionsToEnd()
		{
			while (_rawIL.Position < _rawILLength)
			{
				yield return ReadInstruction();
			}
		}

		private class ResolvableBranchInstruction : IResolvableInstruction
		{
			private static readonly Dictionary<OpCode, OpCode> FullOperations = new Dictionary<OpCode, OpCode>
			{
				[OpCodes.Br_S] = OpCodes.Br,
				[OpCodes.Brtrue_S] = OpCodes.Brtrue,
				[OpCodes.Brfalse_S] = OpCodes.Brfalse,
				[OpCodes.Beq_S] = OpCodes.Beq,
				[OpCodes.Bne_Un_S] = OpCodes.Bne_Un,
				[OpCodes.Blt_S] = OpCodes.Blt,
				[OpCodes.Blt_Un_S] = OpCodes.Blt_Un,
				[OpCodes.Ble_S] = OpCodes.Ble,
				[OpCodes.Ble_Un_S] = OpCodes.Ble_Un,
				[OpCodes.Bgt_S] = OpCodes.Bgt,
				[OpCodes.Bgt_Un_S] = OpCodes.Bgt_Un,
				[OpCodes.Bge_S] = OpCodes.Bge,
				[OpCodes.Bge_Un_S] = OpCodes.Bge_Un,
				[OpCodes.Leave_S] = OpCodes.Leave
			};

			private static readonly Dictionary<OpCode, OpCode> ShortOperations = new Dictionary<OpCode, OpCode>
			{
				[OpCodes.Br] = OpCodes.Br_S,
				[OpCodes.Brtrue] = OpCodes.Brtrue_S,
				[OpCodes.Brfalse] = OpCodes.Brfalse_S,
				[OpCodes.Beq] = OpCodes.Beq_S,
				[OpCodes.Bne_Un] = OpCodes.Bne_Un_S,
				[OpCodes.Blt] = OpCodes.Blt_S,
				[OpCodes.Blt_Un] = OpCodes.Blt_Un_S,
				[OpCodes.Ble] = OpCodes.Ble_S,
				[OpCodes.Ble_Un] = OpCodes.Ble_Un_S,
				[OpCodes.Bgt] = OpCodes.Bgt_S,
				[OpCodes.Bgt_Un] = OpCodes.Bgt_Un_S,
				[OpCodes.Bge] = OpCodes.Bge_S,
				[OpCodes.Bge_Un] = OpCodes.Bge_Un_S,
				[OpCodes.Leave] = OpCodes.Leave_S
			};

			public static ResolvableBranchInstruction FromFull(OpCode operation, int operand, int offset)
			{
				if (!ShortOperations.TryGetValue(operation, out OpCode shortOperation))
				{
					throw new NotSupportedException($"Operation code {operation} was not found in the short branch dictionary.");
				}

				return new ResolvableBranchInstruction(operation, shortOperation, true, operand)
				{
					Offset = offset
				};
			}

			public static ResolvableBranchInstruction FromShort(OpCode operation, sbyte operand, int offset)
			{
				if (!FullOperations.TryGetValue(operation, out OpCode fullOperation))
				{
					throw new NotSupportedException($"Operation code {operation} was not found in the full branch dictionary.");
				}

				return new ResolvableBranchInstruction(fullOperation, operation, false, operand)
				{
					Offset = offset
				};
			}

			private readonly OpCode _fullOperation;

			private readonly OpCode _shortOperation;

			public int BranchOffset { get; }

			public int Size { get; }

			public List<IEmittable> ExceptionHandlers { get; }

			public int Offset { get; set; }

			public OpCode Operation { get; }

			public ResolvableBranchInstruction(OpCode fullOperation, OpCode shortOperation, bool full, int branchOffset)
			{
				_shortOperation = shortOperation;
				_fullOperation = fullOperation;

				if (full)
				{
					Operation = fullOperation;
					Size = Operation.Size + sizeof(int);
				}
				else
				{
					Operation = shortOperation;
					Size = Operation.Size + sizeof(sbyte);
				}

				ExceptionHandlers = new List<IEmittable>();

				BranchOffset = branchOffset;
			}

			public IEmittableInstruction Resolve(IReadOnlyList<IInstruction> instructions)
			{
				BranchInstruction branch = new BranchInstruction(_fullOperation,
					_shortOperation,
					instructions.FindByOffset(Offset + Size + BranchOffset))
				{
					Offset = Offset
				};

				branch.ExceptionHandlers.AddRange(ExceptionHandlers);

				return branch;
			}

			public bool Equals(IInstruction other)
			{
				return this == other as ResolvableBranchInstruction;
			}
		}

		private class ResolvableSwitchInstruction : IResolvableInstruction
		{
			public IReadOnlyList<int> Targets { get; }
			public int Size => Operation.Size + Targets.Count * sizeof(int);

			public List<IEmittable> ExceptionHandlers { get; }

			public int Offset { get; set; }

			public OpCode Operation { get; }

			public ResolvableSwitchInstruction(IReadOnlyList<int> targets, int offset)
			{
				Operation = OpCodes.Switch;

				ExceptionHandlers = new List<IEmittable>();

				Offset = offset;
				Targets = targets;
			}

			public IEmittableInstruction Resolve(IReadOnlyList<IInstruction> instructions)
			{
				SwitchInstruction branch = new SwitchInstruction
				{
					Offset = Offset
				};

				foreach (int target in Targets)
				{
					branch.Targets.Add(instructions.FindByOffset(Offset + Size + target));
				}

				branch.ExceptionHandlers.AddRange(ExceptionHandlers);

				return branch;
			}

			public bool Equals(IInstruction other)
			{
				return this == other as ResolvableSwitchInstruction;
			}
		}
	}
}
