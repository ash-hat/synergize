﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

using Synergize.Instructions;

namespace Synergize.Compilers
{
	public class BodyPostProcessor
	{
		private readonly Dictionary<IInstruction, int> _indexes;
		private readonly List<IEmittableInstruction> _instructions;
		public ILGenerator Generator { get; }

		public Type ReturnValue { get; }

		public BodyPostProcessor(MethodBase site, List<IEmittableInstruction> instructions, ILGenerator generator) : this(site.GetReturnType(), instructions, generator)
		{
		}

		public BodyPostProcessor(Type returnValue, List<IEmittableInstruction> instructions, ILGenerator generator)
		{
			_indexes = new Dictionary<IInstruction, int>();

			ReturnValue = returnValue;
			_instructions = instructions;
			Generator = generator;

			Readjust();
		}

		public void Readjust()
		{
			bool readjusted;
			do
			{
				int offset = 0;
				foreach (IEmittableInstruction il in _instructions)
				{
					il.Offset = offset;
					offset += il.Size;
				}

				readjusted = false;
				foreach (IEmittableInstruction il in _instructions)
				{
					readjusted |= il.Readjust(_instructions, Generator);
				}
			} while (readjusted);

			_indexes.Clear();
			for (int i = 0; i < _instructions.Count; ++i)
			{
				_indexes.Add(_instructions[i], i);
			}
		}

		public void Optimize()
		{
			IEmittableInstruction nop = Instruction.Nop;

			// Redirect branches that target NOPs to the next non-NOP.
			for (int i = 0; i < _instructions.Count; ++i)
			{
				BranchInstruction branchIl = _instructions[i] as BranchInstruction;

				if (branchIl != null && branchIl.TargetInstruction.Equals(nop))
				{
					for (int j = _indexes[branchIl.TargetInstruction] + 1; j < _instructions.Count; ++j)
					{
						IEmittableInstruction targetIl = _instructions[j];

						if (!targetIl.Equals(nop))
						{
							_instructions[i] = new BranchInstruction(branchIl.FullOperation, branchIl.ShortOperation, targetIl);

							break;
						}
					}
				}
			}

			List<IEmittable> exceptionHandlers = new List<IEmittable>();

			// Move all exception handlers off of NOP instructions
			for (int i = 0; i < _instructions.Count; ++i)
			{
				IInstruction il = _instructions[i];

				if (il.Equals(nop))
				{
					exceptionHandlers.AddRange(il.ExceptionHandlers);
					il.ExceptionHandlers.Clear();
				}
				else
				{
					il.ExceptionHandlers.AddRange(exceptionHandlers);
					exceptionHandlers.Clear();
				}
			}

			// Remove all NOP instructions.
			for (int i = _instructions.Count - 1; i >= 0; --i)
			{
				IInstruction il = _instructions[i];

				if (il.Equals(nop))
				{
					_indexes.Remove(il);
					_instructions.RemoveAt(i);
				}
			}

			Readjust();
		}
	}
}
