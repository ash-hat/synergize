﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Synergize.Instructions;
using Synergize.Instructions.Injectors;

namespace Synergize.Compilers
{
	public class Compiler : ICompiler
	{
		public Type ReturnValue { get; }
		public string Name { get; }
		public Type[] Parameters { get; }

		public IInjector Injector { get; }

		public Compiler(Type returnValue, string name, Type[] parameters, IInjector injector)
		{
			ReturnValue = returnValue;
			Name = name;
			Parameters = parameters;

			Injector = injector;
		}

		public MethodInfo Compile()
		{
			DynamicMethod method = new DynamicMethod(Name,
				ReturnValue,
				Parameters,
				true);

			ILGenerator generator = method.GetILGenerator();

			List<IEmittableInstruction> instructions = Injector.Inject(generator).ToList();

			new BodyPostProcessor(ReturnValue, instructions, generator).Optimize();

			foreach (IEmittableInstruction il in instructions)
			{
				il.Emit(generator);
			}

			method.Prepare();

			return method;
		}
	}
}
