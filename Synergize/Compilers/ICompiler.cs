﻿using System;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Compilers
{
	public interface ICompiler
	{
		IInjector Injector { get; }
		string Name { get; }
		Type[] Parameters { get; }

		Type ReturnValue { get; }

		MethodInfo Compile();
	}
}
