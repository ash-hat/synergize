﻿using System;
using System.Collections.Generic;
using System.Linq;

using Synergize.Instructions;

namespace Synergize.Compilers
{
	public class BranchingStackException : Exception
	{
		public BranchingStackException(string message, IEnumerable<BranchInstruction> branches) : base(
			message + $"{Environment.NewLine}Branching context:{Environment.NewLine}{string.Join(Environment.NewLine, branches.Select(x => '\t' + x.ToString()))}")
		{
		}
	}
}
