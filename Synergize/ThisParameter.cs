﻿using System.Reflection;

namespace Synergize
{
	public class ThisParameter : ParameterInfo
	{
		public ThisParameter(MemberInfo method)
		{
			MemberImpl = method;
			ClassImpl = method.DeclaringType;
			NameImpl = "this";
			PositionImpl = -1;
		}
	}
}
