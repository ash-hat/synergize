﻿using System;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class TypeOperandInstruction : OperandInstruction<Type>
	{
		protected override int OperandSize { get; } = 4;

		public TypeOperandInstruction(OpCode operation, Type operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}

		protected override bool OperandEquals(Type other)
		{
			return Operand == other;
		}
	}
}
