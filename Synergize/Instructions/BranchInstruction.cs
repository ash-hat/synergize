﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class BranchInstruction : IEmittableInstruction
	{
		private static void FullEmitter(OpCode operation, int branchOffset, ILGenerator generator)
		{
			generator.Emit(operation, branchOffset);
		}

		private static void ShortEmitter(OpCode operation, int branchOffset, ILGenerator generator)
		{
			generator.Emit(operation, (sbyte) branchOffset);
		}

		private Action<OpCode, int, ILGenerator> Emitter =>
			Shortened
				? (Action<OpCode, int, ILGenerator>) ShortEmitter
				: FullEmitter;
		public int BranchOffset => TargetInstruction.Offset - (Offset + Size);
		public bool Conditional { get; }

		public OpCode FullOperation { get; }
		public bool Shortened { get; private set; }
		public OpCode ShortOperation { get; }
		public int StackDelta { get; }

		public IInstruction TargetInstruction { get; }

		public int Size =>
			Shortened
				? ShortOperation.Size + sizeof(sbyte)
				: FullOperation.Size + sizeof(int);

		public List<IEmittable> ExceptionHandlers { get; }

		public int Offset { get; set; }

		public OpCode Operation => Shortened ? ShortOperation : FullOperation;

		public BranchInstruction(OpCode fullOperation, OpCode shortOperation, IInstruction targetInstruction)
		{
			StackDelta = fullOperation.GetStackDelta();

			ExceptionHandlers = new List<IEmittable>();

			ShortOperation = shortOperation;
			FullOperation = fullOperation;
			Shortened = false;
			Conditional = fullOperation.StackBehaviourPop != StackBehaviour.Pop0;

			TargetInstruction = targetInstruction;
		}

		public bool Readjust(IReadOnlyList<IInstruction> instructions, ILGenerator generator)
		{
			// Cache property.
			int offset = BranchOffset;

			bool wasShortened = Shortened;
			Shortened = sbyte.MinValue <= offset && offset <= sbyte.MaxValue;

			return wasShortened != Shortened;
		}

		public void Emit(ILGenerator generator)
		{
			foreach (IEmittable handler in ExceptionHandlers)
			{
				handler.Emit(generator);
			}

			Emitter(Operation, BranchOffset, generator);
		}

		public bool Equals(IInstruction other)
		{
			BranchInstruction otherBranch = other as BranchInstruction;
			return otherBranch != null
			       && TargetInstruction.Equals(otherBranch.TargetInstruction)
			       && FullOperation == otherBranch.FullOperation
			       && ShortOperation == otherBranch.ShortOperation;
		}

		public override string ToString()
		{
			return
				$"IL_{Offset:x4} {(ExceptionHandlers.Count == 0 ? string.Empty : string.Join(" ", ExceptionHandlers.Select(x => "." + x)) + " ")}{Operation} IL_{TargetInstruction?.Offset ?? Offset + Size + BranchOffset:x4}";
		}
	}
}
