﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public interface IEmittableInstruction : IInstruction, IEmittable
	{
		bool Readjust(IReadOnlyList<IInstruction> instructions, ILGenerator generator);
	}
}
