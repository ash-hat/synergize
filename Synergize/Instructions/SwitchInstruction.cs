﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class SwitchInstruction : IEmittableInstruction
	{
		private static readonly Action<ILGenerator, int> _generatorEnsureCapacity =
			AccessTools.MethodInvocator<ILGenerator, Action<ILGenerator, int>>("EnsureCapacity");

		private static readonly Action<ILGenerator, int> _generatorPutInteger4 =
			AccessTools.MethodInvocator<ILGenerator, Action<ILGenerator, int>>("PutInteger4");

		private int OperandSize => sizeof(int) + Targets.Count * sizeof(int);
		public int StackDelta { get; } = 0;

		public List<IInstruction> Targets { get; }

		public int Size => Operation.Size + OperandSize;

		public List<IEmittable> ExceptionHandlers { get; }

		public int Offset { get; set; }

		public OpCode Operation { get; } = OpCodes.Switch;

		public SwitchInstruction()
		{
			ExceptionHandlers = new List<IEmittable>();

			Targets = new List<IInstruction>();
		}

		public bool Readjust(IReadOnlyList<IInstruction> instructions, ILGenerator generator)
		{
			return false;
		}

		public void Emit(ILGenerator generator)
		{
			foreach (IEmittable handler in ExceptionHandlers)
			{
				handler.Emit(generator);
			}

			generator.Emit(Operation);
			_generatorEnsureCapacity(generator, Targets.Count * sizeof(int));
			foreach (IInstruction target in Targets)
			{
				_generatorPutInteger4(generator, target.Offset - Offset);
			}
		}

		public bool Equals(IInstruction other)
		{
			SwitchInstruction otherSwitch = other as SwitchInstruction;
			return otherSwitch != null && Targets.SequenceEqual(otherSwitch.Targets);
		}

		public override string ToString()
		{
			return
				$"IL_{Offset:x4} {(ExceptionHandlers.Count == 0 ? string.Empty : string.Join(" ", ExceptionHandlers.Select(x => "." + x)) + " ")}{Operation} {string.Join(", ", Targets.Select(x => $"({x})"))}";
		}
	}
}
