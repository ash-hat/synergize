﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class FloatOperandInstruction : EquatableOperandInstruction<float>
	{
		protected override int OperandSize { get; } = sizeof(float);

		public FloatOperandInstruction(OpCode operation, float operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
