﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class UShortOperandInstruction : EquatableOperandInstruction<ushort>
	{
		protected override int OperandSize { get; } = sizeof(ushort);

		public UShortOperandInstruction(OpCode operation, ushort operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, (short) Operand);
		}
	}
}
