﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions
{
	public static class InstructionExtensions
	{
		public static void EmitAll(this IEnumerable<IEmittableInstruction> instructions, ILGenerator generator)
		{
			foreach (IEmittableInstruction il in instructions)
			{
				il.Emit(generator);
			}
		}

		public static IInstruction FindByOffset(this IReadOnlyList<IInstruction> instructions, int offset)
		{
			int min = 0;
			int max = instructions.Count - 1;

			while (min <= max)
			{
				int mid = (min + max) / 2;
				IInstruction instruction = instructions[mid];

				if (instruction.Offset == offset)
				{
					return instruction;
				}

				if (instruction.Offset > offset)
				{
					max = mid - 1;
				}
				else
				{
					min = mid + 1;
				}
			}

			throw new ArgumentException("No instruction matches that offset.", nameof(offset));
		}

		public static MergeInjector Merge(this IInjector original, IInjector other) => new MergeInjector(original, other);

		public static void PrepareAll(this IEnumerable<IInstruction> instructions, ILGenerator generator)
		{
			int offset = 0;
			foreach (IInstruction il in instructions)
			{
				il.Offset = offset;
				offset += il.Size;
			}
		}

		public static InstructionInjector ToInjector(this IEmittableInstruction instruction) => new InstructionInjector(instruction);
	}
}
