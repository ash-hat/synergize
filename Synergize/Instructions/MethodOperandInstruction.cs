﻿using System.Reflection;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class MethodOperandInstruction : OperandInstruction<MethodInfo>
	{
		protected override int OperandSize { get; } = 4;

		public MethodOperandInstruction(OpCode operation, MethodInfo operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}

		protected override bool OperandEquals(MethodInfo other)
		{
			return Operand == other;
		}
	}
}
