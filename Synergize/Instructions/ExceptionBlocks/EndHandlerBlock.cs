﻿using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public class EndHandlerBlock : IEmittable
	{
		public IEmittable StartBlock { get; }

		public EndHandlerBlock(IEmittable startBlock)
		{
			StartBlock = startBlock;
		}

		public void Emit(ILGenerator generator)
		{
			generator.EndExceptionBlock();
		}

		public override string ToString() => "end " + StartBlock;
	}
}
