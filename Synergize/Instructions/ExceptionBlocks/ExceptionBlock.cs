﻿using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public class ExceptionBlock : IEmittable
	{
		public void Emit(ILGenerator generator)
		{
			generator.BeginExceptionBlock();
		}

		public override string ToString() => "try";
	}
}
