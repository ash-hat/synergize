﻿using System.Collections.Generic;

namespace Synergize.Instructions
{
	public interface IResolvableInstruction : IInstruction
	{
		IEmittableInstruction Resolve(IReadOnlyList<IInstruction> instructions);
	}
}
