﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class LongOperandInstruction : EquatableOperandInstruction<long>
	{
		protected override int OperandSize { get; } = sizeof(long);

		public LongOperandInstruction(OpCode operation, long operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
