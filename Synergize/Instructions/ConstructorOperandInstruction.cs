﻿using System.Reflection;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class ConstructorOperandInstruction : OperandInstruction<ConstructorInfo>
	{
		protected override int OperandSize { get; } = 4;

		public ConstructorOperandInstruction(OpCode operation, ConstructorInfo operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}

		protected override bool OperandEquals(ConstructorInfo other)
		{
			return Operand == other;
		}
	}
}
