﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class StringOperandInstruction : EquatableOperandInstruction<string>
	{
		protected override int OperandSize { get; } = 4;

		public StringOperandInstruction(OpCode operation, string operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
