﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class SByteOperandInstruction : EquatableOperandInstruction<sbyte>
	{
		protected override int OperandSize { get; } = sizeof(sbyte);

		public SByteOperandInstruction(OpCode operation, sbyte operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
