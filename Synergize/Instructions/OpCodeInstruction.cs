﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class OpCodeInstruction : IEmittableInstruction
	{
		public virtual int Size => Operation.Size;

		public List<IEmittable> ExceptionHandlers { get; }

		public int Offset { get; set; }

		public OpCode Operation { get; }

		public OpCodeInstruction(OpCode operation)
		{
			ExceptionHandlers = new List<IEmittable>();

			Operation = operation;
		}

		public virtual void Emit(ILGenerator generator)
		{
			foreach (IEmittable handler in ExceptionHandlers)
			{
				handler.Emit(generator);
			}

			generator.Emit(Operation);
		}

		public bool Readjust(IReadOnlyList<IInstruction> instructions, ILGenerator generator)
		{
			return false;
		}

		public virtual bool Equals(IInstruction other)
		{
			OpCodeInstruction opCodeOther = other as OpCodeInstruction;
			return opCodeOther != null && Operation == opCodeOther.Operation;
		}

		public override string ToString()
		{
			return $"IL_{Offset:x4} {(ExceptionHandlers.Count == 0 ? string.Empty : string.Join(" ", ExceptionHandlers.Select(x => "." + x)) + " ")}{Operation.Name}";
		}
	}
}
