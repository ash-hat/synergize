﻿using System;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public abstract class EquatableOperandInstruction<TOperand> : OperandInstruction<TOperand> where TOperand : IEquatable<TOperand>
	{
		protected EquatableOperandInstruction(OpCode operation, TOperand operand) : base(operation, operand)
		{
		}

		protected override bool OperandEquals(TOperand other)
		{
			return Operand.Equals(other);
		}
	}
}
