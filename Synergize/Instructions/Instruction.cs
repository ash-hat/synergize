﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	[SuppressMessage("ReSharper", "IdentifierTypo")]
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public static class Instruction
	{
		public static OpCodeInstruction Add => new OpCodeInstruction(OpCodes.Add);
		public static OpCodeInstruction Add_Ovf => new OpCodeInstruction(OpCodes.Add_Ovf);
		public static OpCodeInstruction Add_Ovf_Un => new OpCodeInstruction(OpCodes.Add_Ovf_Un);
		public static OpCodeInstruction And => new OpCodeInstruction(OpCodes.And);
		public static OpCodeInstruction Arglist => new OpCodeInstruction(OpCodes.Arglist);
		public static OpCodeInstruction Break => new OpCodeInstruction(OpCodes.Break);
		public static OpCodeInstruction Ceq => new OpCodeInstruction(OpCodes.Ceq);
		public static OpCodeInstruction Cgt => new OpCodeInstruction(OpCodes.Cgt);
		public static OpCodeInstruction Cgt_Un => new OpCodeInstruction(OpCodes.Cgt_Un);
		public static OpCodeInstruction Ckfinite => new OpCodeInstruction(OpCodes.Ckfinite);
		public static OpCodeInstruction Clt => new OpCodeInstruction(OpCodes.Clt);
		public static OpCodeInstruction Clt_Un => new OpCodeInstruction(OpCodes.Clt_Un);
		public static OpCodeInstruction Conv_I => new OpCodeInstruction(OpCodes.Conv_I);
		public static OpCodeInstruction Conv_I1 => new OpCodeInstruction(OpCodes.Conv_I1);
		public static OpCodeInstruction Conv_I2 => new OpCodeInstruction(OpCodes.Conv_I2);
		public static OpCodeInstruction Conv_I4 => new OpCodeInstruction(OpCodes.Conv_I4);
		public static OpCodeInstruction Conv_I8 => new OpCodeInstruction(OpCodes.Conv_I8);
		public static OpCodeInstruction Conv_Ovf_I => new OpCodeInstruction(OpCodes.Conv_Ovf_I);
		public static OpCodeInstruction Conv_Ovf_I_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_I_Un);
		public static OpCodeInstruction Conv_Ovf_I1 => new OpCodeInstruction(OpCodes.Conv_Ovf_I1);
		public static OpCodeInstruction Conv_Ovf_I1_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_I1_Un);
		public static OpCodeInstruction Conv_Ovf_I2 => new OpCodeInstruction(OpCodes.Conv_Ovf_I2);
		public static OpCodeInstruction Conv_Ovf_I2_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_I2_Un);
		public static OpCodeInstruction Conv_Ovf_I4 => new OpCodeInstruction(OpCodes.Conv_Ovf_I4);
		public static OpCodeInstruction Conv_Ovf_I4_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_I4_Un);
		public static OpCodeInstruction Conv_Ovf_I8 => new OpCodeInstruction(OpCodes.Conv_Ovf_I8);
		public static OpCodeInstruction Conv_Ovf_I8_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_I8_Un);
		public static OpCodeInstruction Conv_Ovf_U => new OpCodeInstruction(OpCodes.Conv_Ovf_U);
		public static OpCodeInstruction Conv_Ovf_U_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_U_Un);
		public static OpCodeInstruction Conv_Ovf_U1 => new OpCodeInstruction(OpCodes.Conv_Ovf_U1);
		public static OpCodeInstruction Conv_Ovf_U1_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_U1_Un);
		public static OpCodeInstruction Conv_Ovf_U2 => new OpCodeInstruction(OpCodes.Conv_Ovf_U2);
		public static OpCodeInstruction Conv_Ovf_U2_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_U2_Un);
		public static OpCodeInstruction Conv_Ovf_U4 => new OpCodeInstruction(OpCodes.Conv_Ovf_U4);
		public static OpCodeInstruction Conv_Ovf_U4_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_U4_Un);
		public static OpCodeInstruction Conv_Ovf_U8 => new OpCodeInstruction(OpCodes.Conv_Ovf_U8);
		public static OpCodeInstruction Conv_Ovf_U8_Un => new OpCodeInstruction(OpCodes.Conv_Ovf_U8_Un);
		public static OpCodeInstruction Conv_R_Un => new OpCodeInstruction(OpCodes.Conv_R_Un);
		public static OpCodeInstruction Conv_R4 => new OpCodeInstruction(OpCodes.Conv_R4);
		public static OpCodeInstruction Conv_R8 => new OpCodeInstruction(OpCodes.Conv_R8);
		public static OpCodeInstruction Conv_U => new OpCodeInstruction(OpCodes.Conv_U);
		public static OpCodeInstruction Conv_U1 => new OpCodeInstruction(OpCodes.Conv_U1);
		public static OpCodeInstruction Conv_U2 => new OpCodeInstruction(OpCodes.Conv_U2);
		public static OpCodeInstruction Conv_U4 => new OpCodeInstruction(OpCodes.Conv_U4);
		public static OpCodeInstruction Conv_U8 => new OpCodeInstruction(OpCodes.Conv_U8);
		public static OpCodeInstruction Cpblk => new OpCodeInstruction(OpCodes.Cpblk);
		public static OpCodeInstruction Div => new OpCodeInstruction(OpCodes.Div);
		public static OpCodeInstruction Div_Un => new OpCodeInstruction(OpCodes.Div_Un);
		public static OpCodeInstruction Dup => new OpCodeInstruction(OpCodes.Dup);
		public static OpCodeInstruction Initblk => new OpCodeInstruction(OpCodes.Initblk);
		public static OpCodeInstruction Ldlen => new OpCodeInstruction(OpCodes.Ldlen);
		public static OpCodeInstruction Ldnull => new OpCodeInstruction(OpCodes.Ldnull);
		public static OpCodeInstruction Localloc => new OpCodeInstruction(OpCodes.Localloc);
		public static OpCodeInstruction Mul => new OpCodeInstruction(OpCodes.Mul);
		public static OpCodeInstruction Mul_Ovf => new OpCodeInstruction(OpCodes.Mul_Ovf);
		public static OpCodeInstruction Mul_Ovf_Un => new OpCodeInstruction(OpCodes.Mul_Ovf_Un);
		public static OpCodeInstruction Neg => new OpCodeInstruction(OpCodes.Neg);
		public static OpCodeInstruction Nop => new OpCodeInstruction(OpCodes.Nop);
		public static OpCodeInstruction Not => new OpCodeInstruction(OpCodes.Not);
		public static OpCodeInstruction Or => new OpCodeInstruction(OpCodes.Or);
		public static OpCodeInstruction Pop => new OpCodeInstruction(OpCodes.Pop);
		public static OpCodeInstruction Readonly => new OpCodeInstruction(OpCodes.Readonly);
		public static OpCodeInstruction Refanytype => new OpCodeInstruction(OpCodes.Refanytype);
		public static OpCodeInstruction Rem => new OpCodeInstruction(OpCodes.Rem);
		public static OpCodeInstruction Rem_Un => new OpCodeInstruction(OpCodes.Rem_Un);
		public static OpCodeInstruction Ret => new OpCodeInstruction(OpCodes.Ret);
		public static OpCodeInstruction Rethrow => new OpCodeInstruction(OpCodes.Rethrow);
		public static OpCodeInstruction Shl => new OpCodeInstruction(OpCodes.Shl);
		public static OpCodeInstruction Shr => new OpCodeInstruction(OpCodes.Shr);
		public static OpCodeInstruction Shr_Un => new OpCodeInstruction(OpCodes.Shr_Un);
		public static OpCodeInstruction Sub => new OpCodeInstruction(OpCodes.Sub);
		public static OpCodeInstruction Sub_Ovf => new OpCodeInstruction(OpCodes.Sub_Ovf);
		public static OpCodeInstruction Sub_Ovf_Un => new OpCodeInstruction(OpCodes.Sub_Ovf_Un);
		public static OpCodeInstruction Tailcall => new OpCodeInstruction(OpCodes.Tailcall);
		public static OpCodeInstruction Throw => new OpCodeInstruction(OpCodes.Throw);
		public static OpCodeInstruction Volatile => new OpCodeInstruction(OpCodes.Volatile);
		public static OpCodeInstruction Xor => new OpCodeInstruction(OpCodes.Xor);

		private static IEmittableInstruction ByteShortener(byte value, OpCode byteOpcode, OpCode optimizedOpcode0, OpCode optimizedOpcode1, OpCode optimizedOpcode2,
			OpCode optimizedOpcode3)
		{
			switch (value)
			{
				case 0: return new OpCodeInstruction(optimizedOpcode0);
				case 1: return new OpCodeInstruction(optimizedOpcode1);
				case 2: return new OpCodeInstruction(optimizedOpcode2);
				case 3: return new OpCodeInstruction(optimizedOpcode3);
				default: return new ByteOperandInstruction(byteOpcode, value);
			}
		}

		private static IEmittableInstruction IntSByteShortener(int value, OpCode intOpcode, OpCode sbyteOpcode)
		{
			return sbyte.MinValue <= value && value <= sbyte.MaxValue
				? (IEmittableInstruction) new SByteOperandInstruction(sbyteOpcode, (sbyte) value)
				: new IntOperandInstruction(intOpcode, value);
		}

		private static IEmittableInstruction UShortByteShortener(ushort value, OpCode ushortOpcode, OpCode byteOpcode)
		{
			return value <= byte.MaxValue
				? (IEmittableInstruction) new ByteOperandInstruction(byteOpcode, (byte) value)
				: new UShortOperandInstruction(ushortOpcode, value);
		}

		private static IEmittableInstruction UShortByteShortener(ushort value, OpCode ushortOpcode, OpCode byteOpcode, OpCode optimizedOpcode0, OpCode optimizedOpcode1,
			OpCode optimizedOpcode2, OpCode optimizedOpcode3)
		{
			return value <= byte.MaxValue
				? ByteShortener((byte) value, byteOpcode, optimizedOpcode0, optimizedOpcode1, optimizedOpcode2, optimizedOpcode3)
				: new UShortOperandInstruction(ushortOpcode, value);
		}

		public static BranchInstruction Beq(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Beq, OpCodes.Beq_S, target);
		}

		public static BranchInstruction Bge(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Bge, OpCodes.Bge_S, target);
		}

		public static BranchInstruction Bge_Un(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Bge_Un, OpCodes.Bge_Un_S, target);
		}

		public static BranchInstruction Bgt(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Bgt, OpCodes.Bgt_S, target);
		}

		public static BranchInstruction Bgt_Un(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Bgt_Un, OpCodes.Bgt_Un_S, target);
		}

		public static BranchInstruction Ble(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Ble, OpCodes.Ble_S, target);
		}

		public static BranchInstruction Ble_Un(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Ble_Un, OpCodes.Ble_Un_S, target);
		}

		public static BranchInstruction Blt(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Blt, OpCodes.Blt_S, target);
		}

		public static BranchInstruction Blt_Un(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Blt_Un, OpCodes.Blt_Un_S, target);
		}

		public static BranchInstruction Bne_Un(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Bne_Un, OpCodes.Bne_Un_S, target);
		}

		public static BranchInstruction Br(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Br, OpCodes.Br_S, target);
		}

		public static BranchInstruction Brfalse(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Brfalse, OpCodes.Brfalse_S, target);
		}

		public static BranchInstruction Brtrue(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Brtrue, OpCodes.Brtrue_S, target);
		}

		public static MethodOperandInstruction Call(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Call, method);
		}

		public static MethodOperandInstruction Calli(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Calli, method);
		}

		public static MethodOperandInstruction Callvirt(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Callvirt, method);
		}

		public static TypeOperandInstruction Castclass(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Castclass, type);
		}

		public static TypeOperandInstruction Constrained(Type constraint)
		{
			return new TypeOperandInstruction(OpCodes.Constrained, constraint);
		}

		public static TypeOperandInstruction Cpobj(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Cpobj, type);
		}

		public static TypeOperandInstruction Initobj(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Initobj, type);
		}

		public static TypeOperandInstruction Isinst(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Isinst, type);
		}

		public static MethodOperandInstruction Jmp(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Initobj, method);
		}

		public static IEmittableInstruction Ldarg(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Ldarg, OpCodes.Ldarg_S, OpCodes.Ldarg_0, OpCodes.Ldarg_1, OpCodes.Ldarg_2, OpCodes.Ldarg_3);
		}

		public static IEmittableInstruction Ldarga(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Ldarga, OpCodes.Ldarga_S);
		}

		public static IEmittableInstruction Ldc_I4(sbyte constant)
		{
			switch (constant)
			{
				case -1: return new OpCodeInstruction(OpCodes.Ldc_I4_M1);
				case 0: return new OpCodeInstruction(OpCodes.Ldc_I4_0);
				case 1: return new OpCodeInstruction(OpCodes.Ldc_I4_1);
				case 2: return new OpCodeInstruction(OpCodes.Ldc_I4_2);
				case 3: return new OpCodeInstruction(OpCodes.Ldc_I4_3);
				case 4: return new OpCodeInstruction(OpCodes.Ldc_I4_4);
				case 5: return new OpCodeInstruction(OpCodes.Ldc_I4_5);
				case 6: return new OpCodeInstruction(OpCodes.Ldc_I4_6);
				case 7: return new OpCodeInstruction(OpCodes.Ldc_I4_7);
				case 8: return new OpCodeInstruction(OpCodes.Ldc_I4_8);
				default: return new SByteOperandInstruction(OpCodes.Ldc_I4_S, constant);
			}
		}

		public static IEmittableInstruction Ldc_I4(int constant)
		{
			return sbyte.MinValue <= constant && constant <= sbyte.MaxValue
				? Ldc_I4((sbyte) constant)
				: new IntOperandInstruction(OpCodes.Ldc_I4, constant);
		}

		public static LongOperandInstruction Ldc_I8(long constant)
		{
			return new LongOperandInstruction(OpCodes.Ldc_I8, constant);
		}

		public static FloatOperandInstruction Ldc_R4(float constant)
		{
			return new FloatOperandInstruction(OpCodes.Ldc_R4, constant);
		}

		public static DoubleOperandInstruction Ldc_R8(double constant)
		{
			return new DoubleOperandInstruction(OpCodes.Ldc_R8, constant);
		}

		public static IEmittableInstruction Ldelem(Type type)
		{
			if (type.IsPointer)
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I);
			}

			if (type.IsEnum)
			{
				type = type.GetEnumUnderlyingType();
			}

			if (type == typeof(byte) || type == typeof(bool))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_U1);
			}

			if (type == typeof(ushort))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_U2);
			}

			if (type == typeof(uint))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_U4);
			}

			if (type == typeof(ulong))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I8);
			}

			if (type == typeof(sbyte))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I1);
			}

			if (type == typeof(short))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I2);
			}

			if (type == typeof(int))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I4);
			}

			if (type == typeof(long))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_I8);
			}

			if (type == typeof(float))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_R4);
			}

			if (type == typeof(double))
			{
				return new OpCodeInstruction(OpCodes.Ldelem_R8);
			}

			return type.IsValueType || type.IsGenericType
				? new TypeOperandInstruction(OpCodes.Ldelem, type)
				: new OpCodeInstruction(OpCodes.Ldelem_Ref);
		}

		public static TypeOperandInstruction Ldelema(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Ldelema, type);
		}

		public static FieldOperandInstruction Ldfld(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Ldfld, field);
		}

		public static FieldOperandInstruction Ldflda(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Ldflda, field);
		}

		public static MethodOperandInstruction Ldftn(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Ldftn, method);
		}

		public static IEmittableInstruction Ldind(Type type)
		{
			if (type.IsPointer)
			{
				return new OpCodeInstruction(OpCodes.Ldind_I);
			}

			if (type.IsEnum)
			{
				type = type.GetEnumUnderlyingType();
			}

			if (type == typeof(byte) || type == typeof(bool))
			{
				return new OpCodeInstruction(OpCodes.Ldind_U1);
			}

			if (type == typeof(ushort))
			{
				return new OpCodeInstruction(OpCodes.Ldind_U2);
			}

			if (type == typeof(uint))
			{
				return new OpCodeInstruction(OpCodes.Ldind_U4);
			}

			if (type == typeof(ulong))
			{
				return new OpCodeInstruction(OpCodes.Ldind_I8);
			}

			if (type == typeof(sbyte))
			{
				return new OpCodeInstruction(OpCodes.Ldind_I1);
			}

			if (type == typeof(short))
			{
				return new OpCodeInstruction(OpCodes.Ldind_I2);
			}

			if (type == typeof(int))
			{
				return new OpCodeInstruction(OpCodes.Ldind_I4);
			}

			if (type == typeof(long))
			{
				return new OpCodeInstruction(OpCodes.Ldind_I8);
			}

			if (type == typeof(float))
			{
				return new OpCodeInstruction(OpCodes.Ldind_R4);
			}

			if (type == typeof(double))
			{
				return new OpCodeInstruction(OpCodes.Ldind_R8);
			}

			return new TypeOperandInstruction(OpCodes.Ldind_Ref, type);
		}

		public static IEmittableInstruction Ldloc(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Ldloc, OpCodes.Ldloc_S, OpCodes.Ldloc_0, OpCodes.Ldloc_1, OpCodes.Ldloc_2, OpCodes.Ldloc_3);
		}

		public static IEmittableInstruction Ldloc(LocalVariableInfo local)
		{
			return Ldloc((ushort) local.LocalIndex);
		}

		public static IEmittableInstruction Ldloca(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Ldloca, OpCodes.Ldloca_S);
		}

		public static IEmittableInstruction Ldloca(LocalVariableInfo local)
		{
			return Ldloca((ushort) local.LocalIndex);
		}

		public static TypeOperandInstruction Ldobj(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Ldobj, type);
		}

		public static FieldOperandInstruction Ldsfld(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Ldsfld, field);
		}

		public static FieldOperandInstruction Ldsflda(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Ldsflda, field);
		}

		public static StringOperandInstruction Ldstr(string str)
		{
			return new StringOperandInstruction(OpCodes.Ldstr, str);
		}

		public static MethodOperandInstruction Ldtoken(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Ldtoken, method);
		}

		public static FieldOperandInstruction Ldtoken(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Ldtoken, field);
		}

		public static TypeOperandInstruction Ldtoken(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Ldtoken, type);
		}

		public static MethodOperandInstruction Ldvirtftn(MethodInfo method)
		{
			return new MethodOperandInstruction(OpCodes.Ldvirtftn, method);
		}

		public static BranchInstruction Leave(IEmittableInstruction target)
		{
			return new BranchInstruction(OpCodes.Leave, OpCodes.Leave_S, target);
		}

		public static TypeOperandInstruction Mkrefany(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Mkrefany, type);
		}

		public static TypeOperandInstruction Newarr(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Newarr, type);
		}

		public static ConstructorOperandInstruction Newobj(ConstructorInfo ctor)
		{
			return new ConstructorOperandInstruction(OpCodes.Newobj, ctor);
		}

		public static TypeOperandInstruction Refanyval(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Refanyval, type);
		}

		public static TypeOperandInstruction Sizeof(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Sizeof, type);
		}

		public static IEmittableInstruction Starg(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Starg, OpCodes.Starg_S);
		}

		public static IEmittableInstruction Stelem(Type type)
		{
			if (type.IsPointer)
			{
				return new OpCodeInstruction(OpCodes.Stelem_I);
			}

			if (type.IsEnum)
			{
				type = type.GetEnumUnderlyingType();
			}

			if (type == typeof(byte) || type == typeof(bool))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I1);
			}

			if (type == typeof(ushort))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I2);
			}

			if (type == typeof(uint))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I4);
			}

			if (type == typeof(ulong))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I8);
			}

			if (type == typeof(sbyte))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I1);
			}

			if (type == typeof(short))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I2);
			}

			if (type == typeof(int))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I4);
			}

			if (type == typeof(long))
			{
				return new OpCodeInstruction(OpCodes.Stelem_I8);
			}

			if (type == typeof(float))
			{
				return new OpCodeInstruction(OpCodes.Stelem_R4);
			}

			if (type == typeof(double))
			{
				return new OpCodeInstruction(OpCodes.Stelem_R8);
			}

			return new OpCodeInstruction(OpCodes.Stelem_Ref);
		}

		public static FieldOperandInstruction Stfld(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Stfld, field);
		}

		public static IEmittableInstruction Stind(Type type)
		{
			if (type.IsPointer)
			{
				return new OpCodeInstruction(OpCodes.Stind_I);
			}

			if (type.IsEnum)
			{
				type = type.GetEnumUnderlyingType();
			}

			if (type == typeof(byte) || type == typeof(bool))
			{
				return new OpCodeInstruction(OpCodes.Stind_I1);
			}

			if (type == typeof(ushort))
			{
				return new OpCodeInstruction(OpCodes.Stind_I2);
			}

			if (type == typeof(uint))
			{
				return new OpCodeInstruction(OpCodes.Stind_I4);
			}

			if (type == typeof(ulong))
			{
				return new OpCodeInstruction(OpCodes.Stind_I8);
			}

			if (type == typeof(sbyte))
			{
				return new OpCodeInstruction(OpCodes.Stind_I1);
			}

			if (type == typeof(short))
			{
				return new OpCodeInstruction(OpCodes.Stind_I2);
			}

			if (type == typeof(int))
			{
				return new OpCodeInstruction(OpCodes.Stind_I4);
			}

			if (type == typeof(long))
			{
				return new OpCodeInstruction(OpCodes.Stind_I8);
			}

			if (type == typeof(float))
			{
				return new OpCodeInstruction(OpCodes.Stind_R4);
			}

			if (type == typeof(double))
			{
				return new OpCodeInstruction(OpCodes.Stind_R8);
			}

			return new OpCodeInstruction(OpCodes.Stind_Ref);
		}

		public static IEmittableInstruction Stloc(ushort index)
		{
			return UShortByteShortener(index, OpCodes.Stloc, OpCodes.Stloc_S, OpCodes.Stloc_0, OpCodes.Stloc_1, OpCodes.Stloc_2, OpCodes.Stloc_3);
		}

		public static IEmittableInstruction Stloc(LocalVariableInfo local)
		{
			return Stloc((ushort) local.LocalIndex);
		}

		public static TypeOperandInstruction Stobj(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Stobj, type);
		}

		public static FieldOperandInstruction Stsfld(FieldInfo field)
		{
			return new FieldOperandInstruction(OpCodes.Stsfld, field);
		}

		public static ByteOperandInstruction Unaligned(byte alignment)
		{
			if (alignment != 1 && alignment != 2 && alignment != 4)
			{
				throw new ArgumentOutOfRangeException(nameof(alignment), alignment,
					"Alignment must be 1, 2, or 4. See https://docs.microsoft.com/en-us/dotnet/api/system.reflection.emit.opcodes.unaligned for more details.");
			}

			return new ByteOperandInstruction(OpCodes.Unaligned, alignment);
		}

		public static TypeOperandInstruction Unbox(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Unbox, type);
		}

		public static TypeOperandInstruction Unbox_Any(Type type)
		{
			return new TypeOperandInstruction(OpCodes.Unbox_Any, type);
		}
	}
}
