﻿namespace Synergize.Instructions.Injectors
{
	public readonly struct HandledReturn<TReturn>
	{
		public static HandledReturn<TReturn> Unhandled { get; }

		static HandledReturn()
		{
			Unhandled = new HandledReturn<TReturn>(false, default);
		}

		public readonly bool IsHandled;
		public readonly TReturn ReturnValue;

		private HandledReturn(bool isHandled, TReturn returnValue)
		{
			IsHandled = isHandled;
			ReturnValue = returnValue;
		}

		public static implicit operator HandledReturn<TReturn>(TReturn returnValue) => new HandledReturn<TReturn>(true, returnValue);

		public override string ToString() =>
			IsHandled
				? ReturnValue?.ToString() ?? "null"
				: "Unhandled";
	}
}
