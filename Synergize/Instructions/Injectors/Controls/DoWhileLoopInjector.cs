﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

using Synergize.Enumerables;

namespace Synergize.Instructions.Injectors
{
	public class DoWhileLoopInjector : IInjector
	{
		public IInjector Body { get; }
		public IInjector Condition { get; }

		public DoWhileLoopInjector(IInjector condition, IInjector body)
		{
			Condition = condition;
			Body = body;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			// Body must be evaluated BEFORE condition because a stack local might be declared.

			FirstExtractorEnumerable<IEmittableInstruction> bodyFirst = new FirstExtractorEnumerable<IEmittableInstruction>(Body.Inject(generator));
			foreach (IEmittableInstruction il in bodyFirst)
			{
				yield return il;
			}

			FirstExtractorEnumerable<IEmittableInstruction> conditionFirst = new FirstExtractorEnumerable<IEmittableInstruction>(Condition.Inject(generator));
			foreach (IEmittableInstruction il in conditionFirst)
			{
				yield return il;
			}

			if (!conditionFirst.Extracted)
			{
				throw new InvalidOperationException("Condition is empty.");
			}

			yield return Instruction.Brtrue(bodyFirst.Extracted
				? bodyFirst.ExtractedValue
				: conditionFirst.ExtractedValue);
		}
	}
}
