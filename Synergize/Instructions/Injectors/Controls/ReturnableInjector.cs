﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class ReturnableInjector : IInjector
	{
		public IInjector HandledPush { get; }

		public ReturnableInjector(IInjector handledPush)
		{
			HandledPush = handledPush;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			IEmittableInstruction after = Instruction.Nop;

			foreach (IEmittableInstruction il in HandledPush.Inject(generator))
			{
				yield return il;
			}

			yield return Instruction.Brfalse(after);
			yield return Instruction.Ret;

			yield return after;
		}
	}
}
