﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

using Synergize.Enumerables;

namespace Synergize.Instructions.Injectors
{
	public class IfInjector : IInjector
	{
		public IInjector Body { get; }
		public IInjector Condition { get; }
		public IInjector ElseBody { get; }

		public IfInjector(IInjector condition, IInjector body, IInjector elseBody)
		{
			Condition = condition;
			Body = body;
			ElseBody = elseBody;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			IEmittableInstruction after = Instruction.Nop;

			FirstExtractorEnumerable<IEmittableInstruction> conditionFirst = new FirstExtractorEnumerable<IEmittableInstruction>(Condition.Inject(generator));
			foreach (IEmittableInstruction il in conditionFirst)
			{
				yield return il;
			}

			if (!conditionFirst.Extracted)
			{
				throw new InvalidOperationException("Condition is empty.");
			}

			List<IEmittableInstruction> elseBody = ElseBody?.Inject(generator)?.ToList();
			yield return Instruction.Brfalse(elseBody?[0] ?? after);

			foreach (IEmittableInstruction il in Body.Inject(generator))
			{
				yield return il;
			}

			if (elseBody != null)
			{
				yield return Instruction.Br(after);

				foreach (IEmittableInstruction il in elseBody)
				{
					yield return il;
				}
			}

			yield return after;
		}
	}
}
