﻿namespace Synergize.Instructions.Injectors
{
	public interface IWrapper
	{
		IInjector Wrap(IInjector wrapped);
	}
}
