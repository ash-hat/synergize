﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class PostReturnInjector : IInjector
	{
		public IInjector Body { get; }
		public LocalDeclarationInjector ReturnValueDeclaration { get; }

		public PostReturnInjector(IInjector body, LocalDeclarationInjector returnValueDeclaration)
		{
			Body = body;
			ReturnValueDeclaration = returnValueDeclaration;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			foreach (IEmittableInstruction il in ReturnValueDeclaration.Inject(generator))
			{
				yield return il;
			}

			if (ReturnValueDeclaration.Local == null)
			{
				throw new InvalidOperationException("The return value local was supposed to be declared after injection, but it was not.");
			}

			yield return Instruction.Stloc(ReturnValueDeclaration.Local);

			foreach (IEmittableInstruction il in Body.Inject(generator))
			{
				yield return il;
			}

			yield return Instruction.Ldloc(ReturnValueDeclaration.Local);
		}
	}
}
