﻿using System.Reflection;

namespace Synergize.Instructions.Injectors
{
	public interface IStackLocal
	{
		LocalVariableInfo Local { get; }
	}
}
