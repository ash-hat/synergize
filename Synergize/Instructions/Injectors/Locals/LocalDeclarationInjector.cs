﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class LocalDeclarationInjector : IStackLocal, IInjector
	{
		public bool LocalIsPinned { get; }
		public Type LocalType { get; }

		public LocalVariableInfo Local { get; private set; }

		public LocalDeclarationInjector(Type type, bool isPinned)
		{
			if (type == typeof(void))
			{
				throw new ArgumentException(
					"The type of the local cannot be void (this causes a CLR exception which took me 4 days to debug, I wrote this exception immediately after).", nameof(type));
			}

			LocalType = type;
			LocalIsPinned = isPinned;

			Local = null;
		}

		public virtual IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			Local = generator.DeclareLocal(LocalType, LocalIsPinned);
			yield break;
		}
	}
}
