﻿namespace Synergize.Instructions.Injectors
{
	public interface IStackLocalInjector : IInjector
	{
		IStackLocal StackLocal { get; }
	}
}
