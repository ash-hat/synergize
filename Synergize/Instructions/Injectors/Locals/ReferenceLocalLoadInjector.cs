﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class ReferenceStackLocalLoadInjector : IStackLocalInjector
	{
		public IStackLocal StackLocal { get; }

		public ReferenceStackLocalLoadInjector(IStackLocal stackLocal)
		{
			StackLocal = stackLocal;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			if (StackLocal.Local == null)
			{
				throw new InvalidOperationException("The stack local must be injected before attempting to load it.");
			}

			yield return Instruction.Ldloca(StackLocal.Local);
		}
	}
}
