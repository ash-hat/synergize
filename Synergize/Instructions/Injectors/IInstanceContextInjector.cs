﻿namespace Synergize.Instructions.Injectors
{
	public interface IInstanceContextInjector : IInjector
	{
		IInjector Instance { get; }
	}
}
