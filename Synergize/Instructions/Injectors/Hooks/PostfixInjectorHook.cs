﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class PostfixInjectorHook : IInjectorHook
	{
		public IInjector Injector { get; }

		public PostfixInjectorHook(IInjector injector)
		{
			Injector = injector;
		}

		public IEnumerable<IEmittableInstruction> Patch(IReadOnlyList<IEmittableInstruction> instructions, ILGenerator generator)
		{
			IEmittableInstruction ret = Instruction.Ret;

			List<IEmittableInstruction> instructionsList = instructions.ToList();
			List<IEmittableInstruction> injected = Injector.Inject(generator).ToList();

			if (injected.Count == 0)
			{
				foreach (IEmittableInstruction il in instructionsList)
				{
					yield return il;
				}

				yield break;
			}

			// Ignore last one, it is just a ret and we don't need to branch directly after the instruction.
			for (int i = 0; i < instructionsList.Count - 1; i++)
			{
				IEmittableInstruction instruction = instructionsList[i];

				yield return
					instruction.Operation == OpCodes.Ret
						? Instruction.Br(injected[0])
						: instruction;
			}

			foreach (IEmittableInstruction il in injected)
			{
				yield return il;
			}

			yield return ret;
		}
	}
}
