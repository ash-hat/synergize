﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class PatternInjectorHook : IInjectorHook
	{
		private IReadOnlyList<IInstruction> _afterPattern;

		private IReadOnlyList<IInstruction> _beforePattern;

		public IReadOnlyList<IInstruction> AfterPattern
		{
			get => _afterPattern;
			set
			{
				if (value.Count == 0 && _beforePattern.Count == 0)
				{
					throw new ArgumentException("The after pattern cannot be of 0 count if the before pattern is already 0 count (prefix behavior).", nameof(value));
				}

				_afterPattern = value;
			}
		}

		public IReadOnlyList<IInstruction> BeforePattern
		{
			get => _beforePattern;
			set
			{
				if (value.Count == 0 && _afterPattern.Count == 0)
				{
					throw new ArgumentException("The before pattern cannot be of 0 count if the after pattern is already 0 count (postfix behavior).", nameof(value));
				}

				_beforePattern = value;
			}
		}

		public IInjector Injector { get; }

		public PatternInjectorHook(IInjector injector, IReadOnlyList<IInstruction> beforePattern, IReadOnlyList<IInstruction> afterPattern)
		{
			Injector = injector;

			// Have to set the field before accessing BeforePattern, or else a null reference exception will be thrown.
			_afterPattern = afterPattern;

			BeforePattern = beforePattern;
			AfterPattern = afterPattern;
		}

		public IEnumerable<IEmittableInstruction> Patch(IReadOnlyList<IEmittableInstruction> instructions, ILGenerator generator)
		{
			List<IEmittableInstruction> mutable = instructions.ToList();

			int beforeIndex = 0;
			int afterIndex = 0;
			for (int i = 0; i < mutable.Count; i++)
			{
				IEmittableInstruction instruction = mutable[i];

				Console.WriteLine($"0 {_beforePattern.Count} {beforeIndex}");
				if (beforeIndex < BeforePattern.Count)
				{
					if (instruction.Equals(BeforePattern[beforeIndex]))
					{
						++beforeIndex;
					}
					else
					{
						beforeIndex = 0;
					}
				}
				else
				{
					Console.WriteLine($"1 {_afterPattern.Count} {afterIndex}");
					if (AfterPattern.Count == 0 || instruction.Equals(AfterPattern[afterIndex]))
					{
						if (AfterPattern.Count == 0 || ++afterIndex == AfterPattern.Count)
						{
							Console.WriteLine($"2 {_afterPattern.Count} {afterIndex}");
							mutable.InsertRange(i - AfterPattern.Count, Injector.Inject(generator));
							return mutable;
						}
					}
					else
					{
						afterIndex = 0;
					}
				}
			}

			throw new InvalidOperationException("Pattern was never encountered.");
		}
	}
}
