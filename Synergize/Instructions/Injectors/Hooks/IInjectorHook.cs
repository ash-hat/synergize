﻿using Synergize.Transpilers;

namespace Synergize.Instructions.Injectors
{
	public interface IInjectorHook : IPatcher
	{
		IInjector Injector { get; }
	}
}
