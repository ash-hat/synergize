﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportFieldAttribute : MemberImportAttribute
	{
		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			string name = GetName(self);
			FieldInfo field = AccessTools.Field(accessFactory.TypeSite, name);

			if (field == null)
			{
				throw new MissingFieldException(accessFactory.TypeSite.FullName, name);
			}

			if (!self.ParameterType.StripRef(out bool byRef).IsAssignableFrom(field.FieldType))
			{
				throw new ArgumentException("Parameter type is not assignable from the field type.", nameof(self));
			}

			return new InjectorWrapper(byRef ? accessFactory.LoadFieldToReference(field) : accessFactory.LoadField(field), null);
		}
	}
}
