﻿using System.Reflection;

namespace Synergize.Instructions.Injectors
{
	public abstract class MemberImportAttribute : ImportAttribute
	{
		private readonly string _name;

		protected MemberImportAttribute()
		{
			_name = null;
		}

		protected MemberImportAttribute(string name)
		{
			_name = name;
		}

		protected string GetName(ParameterInfo self)
		{
			return _name ?? self.Name;
		}
	}
}
