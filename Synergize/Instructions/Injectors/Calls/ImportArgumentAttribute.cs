﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportArgumentAttribute : ImportAttribute
	{
		public ushort? Index { get; }
		public string Name { get; }

		public ImportArgumentAttribute()
		{
			Index = null;
			Name = null;
		}

		public ImportArgumentAttribute(ushort index)
		{
			Index = index;
			Name = null;
		}

		public ImportArgumentAttribute(string name)
		{
			Index = null;
			Name = name;
		}

		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			ushort index;

			if (Index.HasValue)
			{
				index = Index.Value;
			}
			else
			{
				string argumentName = Name ?? self.Name;
				bool matched = false;

				for (index = 0; index < accessFactory.Parameters.Count; ++index)
				{
					ParameterInfo argument = accessFactory.Parameters[index];
					if (argumentName == argument.Name)
					{
						matched = true;
						break;
					}
				}

				if (!matched)
				{
					throw new ArgumentException("Access factory site does not contain a matching argument.", nameof(accessFactory));
				}
			}

			if (!self.ParameterType.StripRef(out bool selfByRef).IsAssignableFrom(accessFactory.Parameters[index].ParameterType.StripRef(out bool argumentByRef)))
			{
				throw new ArgumentException("Parameter type is not assignable from the matched argument type.", nameof(self));
			}

			return new InjectorWrapper(argumentByRef == selfByRef
					? accessFactory.LoadArgument(index) // ref -> ref or value -> value
					: selfByRef
						? accessFactory.LoadArgumentToReference(index) // value -> ref
						: accessFactory.LoadArgumentFromReference(index), // ref -> value
				null);
		}
	}
}
