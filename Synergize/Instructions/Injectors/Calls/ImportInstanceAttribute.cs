﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportInstanceAttribute : ImportAttribute
	{
		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			IInstanceAccessFactory instanceFactory = accessFactory as IInstanceAccessFactory;
			if (instanceFactory == null)
			{
				throw new ArgumentException("Access factory is not an instance argument factory.", nameof(instanceFactory));
			}

			if (!self.ParameterType.IsAssignableFrom(instanceFactory.TypeSite))
			{
				throw new ArgumentException("Parameter type is not assignable from the access factory site type.", nameof(self));
			}

			return new InjectorWrapper(instanceFactory.LoadInstance, null);
		}
	}
}
