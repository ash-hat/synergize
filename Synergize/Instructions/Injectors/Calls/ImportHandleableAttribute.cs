﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportHandleableAttribute : ImportAttribute
	{
		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			Type valueReturnType = accessFactory.MethodSite.GetReturnType();
			Type handledType = self.ParameterType.StripRef(out bool handledByRef);

			if (!handledByRef)
			{
				throw new InvalidOperationException("The parameter must be a ref parameter.");
			}

			IHandleablePreInjector pre;
			IInjector post;

			if (valueReturnType != typeof(void))
			{
				Type genericHandledType = typeof(HandledReturn<>).MakeGenericType(valueReturnType);
				if (handledType != genericHandledType)
				{
					throw new InvalidOperationException(
						$"The hooked method has a return type of {handledType.FullName}, so the parameter type must be a ref {genericHandledType.FullName}.");
				}

				post = new ValuePostInjector(pre = new ValuePreInjector(genericHandledType), handledType);
			}
			else
			{
				if (handledType != typeof(bool))
				{
					throw new InvalidOperationException("The hooked method has a return type of void, so the parameter type must be a ref bool.");
				}

				post = new VoidPostPreInjector(pre = new VoidPreInjector());
			}

			return new InjectorWrapper(pre, post);
		}

		private interface IHandleablePreInjector : IInjector
		{
			LocalBuilder HandledLocal { get; }
		}

		private class ValuePostInjector : IInjector
		{
			public Type HandledReturnType { get; }
			public IHandleablePreInjector PreInjector { get; }

			public ValuePostInjector(IHandleablePreInjector preInjector, Type handledReturnType)
			{
				PreInjector = preInjector;
				HandledReturnType = handledReturnType;
			}

			public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
			{
				IEmittableInstruction cont = Instruction.Nop;

				yield return Instruction.Ldloc(PreInjector.HandledLocal);
				yield return Instruction.Ldfld(AccessTools.Field(HandledReturnType, nameof(HandledReturn<GenericPlaceholder>.IsHandled)));
				yield return Instruction.Brfalse(cont);

				yield return Instruction.Ldloc(PreInjector.HandledLocal);
				yield return Instruction.Ldfld(AccessTools.Field(HandledReturnType, nameof(HandledReturn<GenericPlaceholder>.ReturnValue)));
				yield return Instruction.Ret;

				yield return cont;
			}
		}

		private class ValuePreInjector : IHandleablePreInjector
		{
			public Type HandledReturnType { get; }
			public LocalBuilder HandledLocal { get; private set; }

			public ValuePreInjector(Type handledReturnType)
			{
				HandledReturnType = handledReturnType;
			}

			public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
			{
				HandledLocal = generator.DeclareLocal(HandledReturnType);

				yield return Instruction.Ldloca(HandledLocal);
				yield return Instruction.Dup;
				yield return Instruction.Initobj(HandledReturnType);
			}
		}

		private class VoidPostPreInjector : IInjector
		{
			public IHandleablePreInjector PreInjector { get; }

			public VoidPostPreInjector(IHandleablePreInjector preInjector)
			{
				PreInjector = preInjector;
			}

			public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
			{
				IEmittableInstruction cont = Instruction.Nop;

				yield return Instruction.Ldloc(PreInjector.HandledLocal);
				yield return Instruction.Brfalse(cont);
				yield return Instruction.Ret;

				yield return cont;
			}
		}

		private class VoidPreInjector : IHandleablePreInjector
		{
			public LocalBuilder HandledLocal { get; private set; }

			public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
			{
				HandledLocal = generator.DeclareLocal(typeof(bool));

				yield return Instruction.Ldc_I4(0);
				yield return Instruction.Stloc(HandledLocal);
				yield return Instruction.Ldloca(HandledLocal);
			}
		}
	}
}
