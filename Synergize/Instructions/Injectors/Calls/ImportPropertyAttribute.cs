﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportPropertyAttribute : MemberImportAttribute
	{
		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			string name = GetName(self);
			PropertyInfo property = AccessTools.Property(accessFactory.TypeSite, name);

			if (property == null)
			{
				throw new MissingPropertyException(accessFactory.TypeSite.FullName, name);
			}

			if (self.ParameterType.IsByRef)
			{
				throw new ArgumentException("Property parameters cannot be passed by reference (ref, in, out).", nameof(self));
			}

			if (!self.ParameterType.IsAssignableFrom(property.PropertyType))
			{
				throw new ArgumentException("Parameter type is not assignable from the property type.", nameof(self));
			}

			return new InjectorWrapper(accessFactory.LoadProperty(property),
				null);
		}
	}
}
