﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	[AttributeUsage(AttributeTargets.Parameter)]
	public abstract class ImportAttribute : Attribute
	{
		public abstract IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self);
	}
}
