﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class MergeInjector : IInjector
	{
		public IInjector ConcatenatedInjector { get; }
		public IInjector OriginalInjector { get; }

		public MergeInjector(IInjector originalInjector, IInjector concatenatedInjector)
		{
			OriginalInjector = originalInjector;
			ConcatenatedInjector = concatenatedInjector;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			foreach (IEmittableInstruction il in OriginalInjector.Inject(generator))
			{
				yield return il;
			}

			foreach (IEmittableInstruction il in ConcatenatedInjector.Inject(generator))
			{
				yield return il;
			}
		}
	}
}
