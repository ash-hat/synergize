﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public interface IInjector
	{
		IEnumerable<IEmittableInstruction> Inject(ILGenerator generator);
	}
}
