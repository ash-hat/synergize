﻿using System;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public class InstanceAccessFactory : AccessFactory, IInstanceAccessFactory
	{
		public IInjector LoadInstance { get; }

		public InstanceAccessFactory(Type typeSite, IInjector loadInstance) : base(typeSite)
		{
			LoadInstance = loadInstance;
		}

		public override IInjector LoadField(FieldInfo field) =>
			field.IsStatic
				? base.LoadField(field)
				: LoadInstance.Merge(Instruction.Ldfld(field).ToInjector());

		public override IInjector LoadFieldToReference(FieldInfo field) =>
			field.IsStatic
				? base.LoadFieldToReference(field)
				: LoadInstance.Merge(Instruction.Ldflda(field).ToInjector());

		public override IInjector StoreField(FieldInfo field, IInjector value) =>
			field.IsStatic
				? base.StoreField(field, value)
				: LoadInstance.Merge(value).Merge(Instruction.Stfld(field).ToInjector());

		public override IInjector LoadProperty(PropertyInfo property) =>
			property.GetMethod.IsStatic
				? base.LoadProperty(property)
				: LoadInstance.Merge(Instruction.Callvirt(property.GetMethod).ToInjector());

		public override IInjector StoreProperty(PropertyInfo property, IInjector value) =>
			property.SetMethod.IsStatic
				? base.StoreProperty(property, value)
				: LoadInstance.Merge(value).Merge(Instruction.Callvirt(property.SetMethod).ToInjector());
	}
}
