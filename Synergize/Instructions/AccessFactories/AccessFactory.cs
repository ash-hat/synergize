﻿using System;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public class AccessFactory : IAccessFactory
	{
		public Type TypeSite { get; }

		public AccessFactory(Type typeSite)
		{
			TypeSite = typeSite;
		}

		public virtual IInjector LoadField(FieldInfo field)
		{
			CheckStaticFieldInfo(field);

			return Instruction.Ldfld(field).ToInjector();
		}

		public IInjector LoadField(string name) => LoadField(GetField(name));

		public virtual IInjector LoadFieldToReference(FieldInfo field)
		{
			CheckStaticFieldInfo(field);

			return Instruction.Ldflda(field).ToInjector();
		}

		public IInjector LoadFieldToReference(string name) => LoadFieldToReference(GetField(name));

		public virtual IInjector StoreField(FieldInfo field, IInjector value)
		{
			CheckStaticFieldInfo(field);

			return Instruction.Stfld(field).ToInjector();
		}

		public IInjector StoreField(string name, IInjector value) => StoreField(GetField(name), value);

		public IInjector StoreFieldFromReference(FieldInfo field, IInjector value)
		{
			CheckStaticFieldInfo(field);

			return StoreField(field, value.Merge(Instruction.Ldind(field.FieldType).ToInjector()));
		}

		public IInjector StoreFieldFromReference(string name, IInjector value) => StoreFieldFromReference(GetField(name), value);

		public virtual IInjector LoadProperty(PropertyInfo property)
		{
			CheckStaticPropertyInfo(property.GetMethod);

			return Instruction.Call(property.GetMethod).ToInjector();
		}

		public IInjector LoadProperty(string name) => LoadProperty(GetProperty(name));

		public virtual IInjector StoreProperty(PropertyInfo property, IInjector value)
		{
			CheckStaticPropertyInfo(property.SetMethod);

			return value.Merge(Instruction.Call(property.SetMethod).ToInjector());
		}

		public IInjector StoreProperty(string name, IInjector value) => StoreProperty(GetProperty(name), value);

		public IInjector StorePropertyFromReference(PropertyInfo property, IInjector value)
		{
			CheckStaticPropertyInfo(property.SetMethod);

			return StoreProperty(property, value.Merge(Instruction.Ldind(property.PropertyType).ToInjector()));
		}

		public IInjector StorePropertyFromReference(string name, IInjector value) => StorePropertyFromReference(GetProperty(name), value);

		protected virtual FieldInfo GetField(string name)
		{
			FieldInfo field = AccessTools.Field(TypeSite, name);

			if (field == null)
			{
				throw new MissingFieldException(TypeSite.FullName, name);
			}

			return field;
		}

		protected virtual PropertyInfo GetProperty(string name)
		{
			PropertyInfo property = AccessTools.Property(TypeSite, name);

			if (property == null)
			{
				throw new MissingPropertyException(TypeSite.FullName, name);
			}

			return property;
		}

		protected void CheckMemberInfo(MemberInfo member)
		{
			if (!member.DeclaringType.IsAssignableFrom(TypeSite))
			{
				throw new ArgumentException("Member is not within the type site.", nameof(member));
			}
		}

		protected virtual void CheckStaticFieldInfo(FieldInfo field)
		{
			CheckMemberInfo(field);

			if (!field.IsStatic)
			{
				throw new ArgumentException("Field is non-static.", nameof(field));
			}
		}

		protected virtual void CheckStaticPropertyInfo(MethodInfo propertyMethod)
		{
			CheckMemberInfo(propertyMethod);

			if (!propertyMethod.IsStatic)
			{
				throw new ArgumentException("Property is non-static.", nameof(propertyMethod));
			}
		}
	}
}
