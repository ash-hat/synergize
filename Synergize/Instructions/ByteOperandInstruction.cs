﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class ByteOperandInstruction : EquatableOperandInstruction<byte>
	{
		protected override int OperandSize { get; } = sizeof(byte);

		public ByteOperandInstruction(OpCode operation, byte operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
