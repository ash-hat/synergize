﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class ShortOperandInstruction : EquatableOperandInstruction<short>
	{
		protected override int OperandSize { get; } = sizeof(short);

		public ShortOperandInstruction(OpCode operation, short operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
