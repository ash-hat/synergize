﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Synergize.Enumerables
{
	public abstract class ExtractorEnumerable<T> : IExtractorEnumerable<T>
	{
		private T _extractedValue;

		public bool Extracted { get; private set; }

		public IEnumerable<T> ExtractionTarget { get; }

		public T ExtractedValue
		{
			get
			{
				if (!Extracted)
				{
					throw new InvalidOperationException("Extractor has not set the extracted yet.");
				}

				return _extractedValue;
			}
			protected set
			{
				_extractedValue = value;
				Extracted = true;
			}
		}

		protected ExtractorEnumerable(IEnumerable<T> extractionTarget)
		{
			_extractedValue = default;

			ExtractionTarget = extractionTarget;
		}

		public IEnumerator<T> GetEnumerator()
		{
			Extracted = false;
			return Enumerate();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		protected abstract IEnumerator<T> Enumerate();
	}
}
