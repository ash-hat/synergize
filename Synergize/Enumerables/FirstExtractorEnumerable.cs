﻿using System.Collections.Generic;

namespace Synergize.Enumerables
{
	public class FirstExtractorEnumerable<T> : ExtractorEnumerable<T>
	{
		public FirstExtractorEnumerable(IEnumerable<T> extractionTarget) : base(extractionTarget)
		{
		}

		protected override IEnumerator<T> Enumerate()
		{
			T first;
			using (IEnumerator<T> extraction = ExtractionTarget.GetEnumerator())
			{
				if (!extraction.MoveNext())
				{
					yield break;
				}

				first = extraction.Current;

				do
				{
					yield return extraction.Current;
				} while (extraction.MoveNext());
			}

			ExtractedValue = first;
		}
	}
}
