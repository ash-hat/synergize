﻿using System.Collections.Generic;

namespace Synergize.Enumerables
{
	public class LastExtractorEnumerable<T> : ExtractorEnumerable<T>
	{
		public LastExtractorEnumerable(IEnumerable<T> extractionTarget) : base(extractionTarget)
		{
		}

		protected override IEnumerator<T> Enumerate()
		{
			using (IEnumerator<T> extraction = ExtractionTarget.GetEnumerator())
			{
				bool yielded = false;

				while (extraction.MoveNext())
				{
					yielded = true;

					yield return extraction.Current;
				}

				if (yielded)
				{
					ExtractedValue = extraction.Current;
				}
			}
		}
	}
}
