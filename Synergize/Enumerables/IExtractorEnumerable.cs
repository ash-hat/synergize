﻿using System.Collections.Generic;

namespace Synergize.Enumerables
{
	public interface IExtractorEnumerable<out T> : IEnumerable<T>
	{
		T ExtractedValue { get; }
		IEnumerable<T> ExtractionTarget { get; }
	}
}
