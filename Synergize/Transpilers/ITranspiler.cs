﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Synergize.Transpilers
{
	public interface ITranspiler : IDisposable
	{
		IPatcher Patcher { get; }
		MethodBase Site { get; }

		IDisposable CreateHandle();
		IDisposable CreateHandle(StackFrame callingFrame);
	}
}
