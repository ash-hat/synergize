﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Synergize.Compilers;
using Synergize.Instructions;
using Synergize.Redirection;

namespace Synergize.Transpilers
{
	public class Transpiler : ITranspiler
	{
		private readonly Type[] _parameterTypes;
		private readonly MethodBodyReader _reader;

		private TranspilerRedirectionHandle _currentHandle;

		protected bool IsDisposed { get; set; }
		public Type ReturnType { get; }

		public MethodBase Site { get; }
		public IPatcher Patcher { get; }

		public Transpiler(MethodBase site, IPatcher patcher)
		{
			Site = site;
			ReturnType = site.GetReturnType();
			Patcher = patcher;

			_reader = new MethodBodyReader(site);

			if (ReturnType.IsByRef)
			{
				throw new NotSupportedException("Dynamic methods do not support return types marked as ref.");
			}

			_parameterTypes = _reader.Parameters.Select(x => x.ParameterType).ToArray();
		}

		~Transpiler()
		{
			Dispose(false);
		}

		public IDisposable CreateHandle(StackFrame callingFrame)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(nameof(Transpiler));
			}

			if (_currentHandle != null)
			{
				throw new InvalidOperationException("A handle has already been created and must be disposed of before creating another.");
			}

			DynamicMethod method = DuplicateOriginal(callingFrame);
			ILGenerator generator = method.GetILGenerator();

			List<IEmittableInstruction> instructions = Patcher.Patch(_reader.Instructions, generator).ToList();

#if DEBUG
			Console.WriteLine($"Patched transpiled method {Site.ToFullString()}:");
			Console.WriteLine();

			foreach (IEmittableInstruction il in instructions)
			{
				Console.WriteLine(il);
			}
			
			Console.WriteLine();
#endif

			new BodyPostProcessor(Site, instructions, generator).Optimize();

#if DEBUG
			Console.WriteLine($"Emitting transpiled method {Site.ToFullString()}:");
			Console.WriteLine();
#endif

			foreach (IEmittableInstruction il in instructions)
			{
#if DEBUG
				Console.WriteLine(il);
#endif
				il.Emit(generator);
			}

#if DEBUG
				Console.WriteLine();
#endif

			method.Prepare();

			return _currentHandle = new TranspilerRedirectionHandle(this, Redirector.Redirect(Site, method));
		}

		public IDisposable CreateHandle() => CreateHandle(new StackTrace().GetFrame(1));

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected DynamicMethod DuplicateOriginal(StackFrame callingFrame)
		{
			string duplicateName = Site.Name + "[Synergized:" + callingFrame.GetMethod().ToTypeString() + ":IL_" + callingFrame.GetILOffset().ToString("x4") + "]";

			DynamicMethod patched =
				new DynamicMethod(duplicateName,
					MethodAttributes.Public | MethodAttributes.Static,
					CallingConventions.Standard,
					ReturnType,
					_parameterTypes,
					Site.DeclaringType,
					true);

			ILGenerator generator = patched.GetILGenerator();

			for (int i = 0; i < _reader.Parameters.Length; ++i)
			{
				patched.DefineParameter(i, _reader.Parameters[i].Attributes, _reader.Parameters[i].Name);
			}

			foreach (LocalVariableInfo local in _reader.LocalVariables)
			{
				generator.DeclareLocal(local.LocalType, local.IsPinned);
			}

			return patched;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (IsDisposed)
			{
				return;
			}

			IsDisposed = true;
		}

		private sealed class TranspilerRedirectionHandle : IDisposable
		{
			private readonly IDisposable _redirection;
			private readonly Transpiler _transpiler;
			private bool _disposed;

			public TranspilerRedirectionHandle(Transpiler transpiler, IDisposable redirection)
			{
				_transpiler = transpiler;
				_redirection = redirection;
			}

			public void Dispose()
			{
				if (_disposed)
				{
					return;
				}

				_disposed = true;

				_redirection.Dispose();
				_transpiler._currentHandle = null;
			}
		}
	}
}
