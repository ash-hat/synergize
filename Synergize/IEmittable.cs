﻿using System.Reflection.Emit;

namespace Synergize
{
	public interface IEmittable
	{
		void Emit(ILGenerator generator);
	}
}
