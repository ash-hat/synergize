﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Synergize.Redirection
{
	public class MonoRedirectionEnvironment : IRedirectionEnvironment
	{
		public IRedirectionEnvironment Environment { get; }

		public MonoRedirectionEnvironment(IRedirectionEnvironment environment)
		{
			Environment = environment;
		}

		public unsafe void Prepare(MethodBase method)
		{
			*((ushort*) method.MethodHandle.Value + 1) |= (ushort) MethodImplOptions.NoInlining;
		}

		public IDisposable AllowWrite(IntPtr start, uint size)
		{
			return Environment.AllowWrite(start, size);
		}

		public IDisposable AllowExecute(IntPtr start, uint size)
		{
			return Environment.AllowExecute(start, size);
		}

		public void FlushInstructions(IntPtr start, uint size)
		{
			Environment.FlushInstructions(start, size);
		}
	}
}
