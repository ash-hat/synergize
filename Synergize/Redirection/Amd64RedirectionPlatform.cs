﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public unsafe class Amd64RedirectionPlatform : IRedirectionPlatform
	{
		public IRedirectionEnvironment Environment { get; }

		public Amd64RedirectionPlatform(IRedirectionEnvironment environment)
		{
			Environment = environment;
		}

		public IDisposable Redirect(MethodBase original, MethodBase replacement)
		{
			const uint detourSize = 2 + sizeof(long) + 2;

			byte* originalPtr = (byte*) original.GetRuntimeMethodHandle().GetFunctionPointer().ToInt64();
			byte* replacementPtr = (byte*) replacement.GetRuntimeMethodHandle().GetFunctionPointer().ToInt64();
			originalPtr = GetPointerFromFunction(originalPtr);

			IntPtr originalIntPtr = new IntPtr(originalPtr);
			IDisposable handle = new BlockCopyHandle(originalPtr, detourSize, replacement)
				.MergeDispose(Environment.AllowWrite(originalIntPtr, detourSize))
				.MergeDispose(Environment.AllowExecute(originalIntPtr, detourSize));

			*originalPtr++ = 0x48;
			*originalPtr++ = 0xB8;
			*(long*) originalPtr = (long) replacementPtr;
			originalPtr += sizeof(long);
			*originalPtr++ = 0xFF;
			*originalPtr = 0xE0;

			Environment.FlushInstructions(originalIntPtr, detourSize);

			return handle;
		}

		private byte* GetPointerFromFunction(byte* function)
		{
			while (*function == 0xE9)
			{
				++function;

				int jmpOffset = *(int*) function;
				function += sizeof(int);

				function += jmpOffset;
			}

			return function;
		}
	}
}
