﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public interface IRedirectionPlatform
	{
		IRedirectionEnvironment Environment { get; }

		IDisposable Redirect(MethodBase original, MethodBase replacement);
	}
}
