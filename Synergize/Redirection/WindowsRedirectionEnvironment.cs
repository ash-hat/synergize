﻿#if (WINDOWS || ANY)
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Synergize.Redirection
{
	public class WindowsRedirectionEnvironment : IRedirectionEnvironment
	{
		[Flags]
		public enum AllocationType
		{
			// Core
			Commit = 0x00001000,
			Reserve = 0x00002000,
			Reset = 0x00080000,
			ResetUndo = 0x01000000,

			// Modifiers
			LargePages = 0x20000000,
			Physical = 0x00400000,
			TopDown = 0x00100000,
			WriteWatch = 0x00200000
		}

		[Flags]
		public enum FreeType
		{
			// Core
			Decommit = 0x00004000,
			Release = 0x00008000,

			// Modifiers
			CoalescePlaceholders = 0x20000000,
			PreservePlaceholder = 0x00000002
		}

		[Flags]
		public enum PageProtection
		{
			// Core (flags must contain one of these)
			Execute = 0x10,
			ExecuteRead = 0x20,
			ExecuteReadWrite = 0x40,
			ExecuteWriteCopy = 0x80,
			NoAccess = 0x01,
			Readonly = 0x02,
			ReadWrite = 0x04,
			WriteCopy = 0x08,

			// Modifiers
			Guard = 0x100,
			NoCache = 0x200,
			WriteCombine = 0x400,

			// Errors
			PageTargetsInvalid = 0x40000000,
			PageTargetsNoUpdate = 0x40000000
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool FlushInstructionCache(IntPtr hProcess, IntPtr lpBaseAddress, uint dwSize);

		[DllImport("kernel32.dll", SetLastError = true)]
		private static extern bool VirtualProtect(IntPtr lpAddress, uint dwSize, PageProtection flNewProtect, out PageProtection lpflOldProtect);

		public void Prepare(MethodBase method)
		{
		}

		public IDisposable AllowWrite(IntPtr start, uint size)
		{
			// MonoMod had issues with just ReadWrite
			return AllowExecute(start, size);
		}

		public IDisposable AllowExecute(IntPtr start, uint size)
		{
			if (!VirtualProtect(start, size, PageProtection.ExecuteReadWrite, out PageProtection oldProtect))
			{
				throw new Win32Exception();
			}

			return new ProtectionHandle(start, size, oldProtect);
		}

		public void FlushInstructions(IntPtr start, uint size)
		{
			if (!FlushInstructionCache(new IntPtr(Process.GetCurrentProcess().Id), start, size))
			{
				throw new Win32Exception();
			}
		}

		private class ProtectionHandle : IDisposable
		{
			private readonly PageProtection _protection;
			private readonly uint _size;
			private readonly IntPtr _start;

			public ProtectionHandle(IntPtr start, uint size, PageProtection protection)
			{
				_start = start;
				_size = size;
				_protection = protection;
			}

			public void Dispose()
			{
				if (!VirtualProtect(_start, _size, _protection, out _))
				{
					throw new Win32Exception();
				}
			}
		}
	}
}
#endif
