﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public unsafe class BlockCopyHandle : IDisposable
	{
		private readonly byte[] _originalData;
		private readonly byte* _pointer;

		// Prevents GC
		// ReSharper disable once NotAccessedField.Local
		private MethodBase _replacement;

		public BlockCopyHandle(byte* pointer, long size, MethodBase replacement)
		{
			_pointer = pointer;
			_replacement = replacement;

			_originalData = new byte[size];

			for (int i = 0; i < size; ++i)
			{
				_originalData[i] = *(pointer + i);
			}
		}

		public void Dispose()
		{
			for (int i = 0; i < _originalData.Length; ++i)
			{
				*(_pointer + i) = _originalData[i];
			}

			_replacement = null;
		}
	}
}
