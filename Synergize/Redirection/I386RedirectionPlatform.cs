﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public unsafe class I386RedirectionPlatform : IRedirectionPlatform
	{
		public IRedirectionEnvironment Environment { get; }

		public I386RedirectionPlatform(IRedirectionEnvironment environment)
		{
			Environment = environment;
		}

		public IDisposable Redirect(MethodBase original, MethodBase replacement)
		{
			const uint detourSize = 1 + sizeof(int);

			byte* originalPtr = (byte*) original.GetRuntimeMethodHandle().GetFunctionPointer().ToInt32();

			IntPtr originalIntPtr = new IntPtr(originalPtr);
			IDisposable handle =
				new BlockCopyHandle(originalPtr, detourSize, replacement)
					.MergeDispose(Environment.AllowWrite(originalIntPtr, detourSize))
					.MergeDispose(Environment.AllowExecute(originalIntPtr, detourSize));

			*originalPtr++ = 0x68;
			*(int*) originalPtr = replacement.GetRuntimeMethodHandle().GetFunctionPointer().ToInt32();
			originalPtr += sizeof(int);
			*originalPtr = 0xC3;

			Environment.FlushInstructions(originalIntPtr, detourSize);

			return handle;
		}
	}
}
