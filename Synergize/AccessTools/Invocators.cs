﻿using System;
using System.Reflection;

using Synergize.Compilers;
using Synergize.Instructions;
using Synergize.Instructions.Injectors;

namespace Synergize
{
	public static partial class AccessTools
	{
		#region Fields

		private static string GetFieldAccessorName(FieldInfo field)
		{
			string name = string.Empty;

			if (field.DeclaringType != null)
			{
				name += field.DeclaringType.FullName + ".";
			}

			name += "SynergizeAccessor_" + field.Name;

			return name;
		}

		public static Compiler FieldGetterInvocatorCompiler(FieldInfo field)
		{
			Type[] parameters;
			IEmittableInstruction[] il;

			if (field.IsStatic)
			{
				parameters = new Type[0];

				il = new IEmittableInstruction[]
				{
					Instruction.Ldsfld(field), Instruction.Ret
				};
			}
			else
			{
				parameters = new[]
				{
					field.DeclaringType
				};

				il = new[]
				{
					Instruction.Ldarg(0), Instruction.Ldfld(field), Instruction.Ret
				};
			}

			return new Compiler(field.FieldType, GetFieldAccessorName(field), parameters, new EnumerableInjector(il));
		}

		public static Compiler FieldSetterInvocatorCompiler(FieldInfo field)
		{
			Type[] parameters;
			IEmittableInstruction[] il;

			if (field.IsStatic)
			{
				parameters = new[]
				{
					field.FieldType
				};

				il = new[]
				{
					Instruction.Ldarg(0), Instruction.Stsfld(field), Instruction.Ret
				};
			}
			else
			{
				parameters = new[]
				{
					field.DeclaringType, field.FieldType
				};

				il = new[]
				{
					Instruction.Ldarg(0), Instruction.Ldarg(1), Instruction.Stfld(field), Instruction.Ret
				};
			}

			return new Compiler(field.FieldType, GetFieldAccessorName(field), parameters, new EnumerableInjector(il));
		}

		#region Getter Invocators

		// ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
		private static void FieldGetterChecks<TValue>(FieldInfo field)
		{
			if (!typeof(TValue).IsAssignableFrom(field.FieldType))
			{
				throw new ArgumentException("Value type is not assignable from the field type.", nameof(TValue));
			}
		}

		public static Func<TValue> FieldGetterInvocator<TValue>(FieldInfo field)
		{
			FieldGetterChecks<TValue>(field);

			if (!field.IsStatic)
			{
				throw new ArgumentException("Field is non-static.", nameof(field));
			}

			return FieldGetterInvocatorCompiler(field).Compile().CreateDelegate<Func<TValue>>();
		}

		public static Func<TValue> FieldGetterInvocator<TValue>(Type type, string name)
		{
			return FieldGetterInvocator<TValue>(Field(type, name));
		}

		public static Func<TValue> FieldGetterInvocator<TDeclarer, TValue>(string name)
		{
			return FieldGetterInvocator<TValue>(typeof(TDeclarer), name);
		}

		public static Func<TInstance, TValue> FieldGetterInvocator<TInstance, TValue>(FieldInfo field)
		{
			FieldGetterChecks<TValue>(field);

			if (field.IsStatic)
			{
				throw new ArgumentException("Field is static.", nameof(field));
			}

			if (field.DeclaringType == null)
			{
				throw new ArgumentException("Declaring type is null.", nameof(field));
			}

			if (!field.DeclaringType.IsAssignableFrom(typeof(TInstance)))
			{
				throw new ArgumentException("Declaring type is not assignable from the instance type.", nameof(TInstance));
			}

			return FieldGetterInvocatorCompiler(field).Compile().CreateDelegate<Func<TInstance, TValue>>();
		}

		#endregion

		#region Setter Invocators

		// ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
		private static void FieldSetterChecks<TValue>(FieldInfo field)
		{
			if (!field.FieldType.IsAssignableFrom(typeof(TValue)))
			{
				throw new ArgumentException("Field type is not assignable from the value type.", nameof(TValue));
			}
		}

		public static Action<TValue> FieldSetterInvocator<TValue>(FieldInfo field)
		{
			FieldSetterChecks<TValue>(field);

			if (!field.IsStatic)
			{
				throw new ArgumentException("Field is non-static.", nameof(field));
			}

			return FieldSetterInvocatorCompiler(field).Compile().CreateDelegate<Action<TValue>>();
		}

		public static Action<TValue> FieldSetterInvocator<TValue>(Type type, string name)
		{
			return FieldSetterInvocator<TValue>(Field(type, name));
		}

		public static Action<TValue> FieldSetterInvocator<TDeclarer, TValue>(string name)
		{
			return FieldSetterInvocator<TValue>(typeof(TDeclarer), name);
		}

		public static Action<TInstance, TValue> FieldSetterInvocator<TInstance, TValue>(FieldInfo field)
		{
			FieldSetterChecks<TValue>(field);

			if (field.IsStatic)
			{
				throw new ArgumentException("Field is static.", nameof(field));
			}

			if (field.DeclaringType == null)
			{
				throw new ArgumentException("Declaring type is null.", nameof(field));
			}

			if (!field.DeclaringType.IsAssignableFrom(typeof(TInstance)))
			{
				throw new ArgumentException("Declaring type is not assignable from the instance type.", nameof(TInstance));
			}

			return FieldSetterInvocatorCompiler(field).Compile().CreateDelegate<Action<TInstance, TValue>>();
		}

		public static Action<TInstance, TValue> FieldSetterInvocator<TInstance, TValue>(Type type, string name)
		{
			return FieldSetterInvocator<TInstance, TValue>(Field(type, name));
		}

		public static Action<TInstance, TValue> FieldSetterInvocator<TDeclarer, TInstance, TValue>(string name)
		{
			return FieldSetterInvocator<TInstance, TValue>(typeof(TDeclarer), name);
		}

		#endregion

		#endregion

		#region Properties

		#region Getter Invocators

		public static Func<TValue> PropertyGetterInvocator<TValue>(PropertyInfo property)
		{
			if (property.GetMethod == null)
			{
				throw new ArgumentException("Property has no getter.", nameof(property));
			}

			if (!property.GetMethod.IsStatic)
			{
				throw new ArgumentException("Property is non-static.", nameof(property));
			}

			return MethodInvocator<Func<TValue>>(property.GetMethod);
		}

		public static Func<TValue> PropertyGetterInvocator<TValue>(Type type, string name)
		{
			return PropertyGetterInvocator<TValue>(Property(type, name));
		}

		public static Func<TValue> PropertyGetterInvocator<TDeclarer, TValue>(string name)
		{
			return PropertyGetterInvocator<TValue>(typeof(TDeclarer), name);
		}

		public static Func<TInstance, TValue> PropertyGetterInvocator<TInstance, TValue>(PropertyInfo property)
		{
			if (property.GetMethod == null)
			{
				throw new ArgumentException("Property has no getter.", nameof(property));
			}

			if (property.GetMethod.IsStatic)
			{
				throw new ArgumentException("Property is static.", nameof(property));
			}

			return MethodInvocator<Func<TInstance, TValue>>(property.GetMethod);
		}

		public static Func<TInstance, TValue> PropertyGetterInvocator<TInstance, TValue>(Type type, string name)
		{
			return PropertyGetterInvocator<TInstance, TValue>(Property(type, name));
		}

		public static Func<TInstance, TValue> PropertyGetterInvocator<TDeclarer, TInstance, TValue>(string name)
		{
			return PropertyGetterInvocator<TInstance, TValue>(typeof(TDeclarer), name);
		}

		#endregion

		#region Setter Invocators

		public static Action<TValue> PropertySetterInvocator<TValue>(PropertyInfo property)
		{
			if (property.SetMethod == null)
			{
				throw new ArgumentException("Property has no setter.", nameof(property));
			}

			if (!property.SetMethod.IsStatic)
			{
				throw new ArgumentException("Property is non-static.", nameof(property));
			}

			return MethodInvocator<Action<TValue>>(property.SetMethod);
		}

		public static Action<TValue> PropertySetterInvocator<TValue>(Type type, string name)
		{
			return PropertySetterInvocator<TValue>(Property(type, name));
		}

		public static Action<TValue> PropertySetterInvocator<TDeclarer, TValue>(string name)
		{
			return PropertySetterInvocator<TValue>(typeof(TDeclarer), name);
		}

		public static Action<TInstance, TValue> PropertySetterInvocator<TInstance, TValue>(PropertyInfo property)
		{
			if (property.SetMethod == null)
			{
				throw new ArgumentException("Property has no setter.", nameof(property));
			}

			if (!property.SetMethod.IsStatic)
			{
				throw new ArgumentException("Property is static.", nameof(property));
			}

			return MethodInvocator<Action<TInstance, TValue>>(property.SetMethod);
		}

		public static Action<TInstance, TValue> PropertySetterInvocator<TInstance, TValue>(Type type, string name)
		{
			return PropertySetterInvocator<TInstance, TValue>(Property(type, name));
		}

		public static Action<TInstance, TValue> PropertySetterInvocator<TDeclarer, TInstance, TValue>(string name)
		{
			return PropertySetterInvocator<TInstance, TValue>(typeof(TDeclarer), name);
		}

		#endregion

		#endregion

		#region Methods

		public static Compiler MethodInvocatorCompiler(MethodBase site, Type returnValue, params Type[] parameters)
		{
			if (!returnValue.IsAssignableFrom(site.GetReturnType()))
			{
				throw new ArgumentException("The calling return type is not assignable from the method return type.", nameof(returnValue));
			}

			ParameterInfo[] parameterInfos = site.GetParameters();
			int parameterOffset = site.IsStatic ? 0 : 1;

			if (parameterInfos.Length + parameterOffset != parameters.Length)
			{
				throw new ArgumentException("The count of calling parameter types is not the same as the full method parameter count.");
			}

			if (!site.IsStatic)
			{
				if (site.DeclaringType == null)
				{
					throw new NotSupportedException("A method cannot be non-static without a declaring type.");
				}

				if (!site.DeclaringType.IsAssignableFrom(parameters[0]))
				{
					throw new ArgumentException("The declaring type of the method is not assignable from the instance (first parameter) type.", nameof(parameters));
				}
			}

			for (int i = 0; i < parameterInfos.Length; ++i)
			{
				if (!parameterInfos[i].ParameterType.IsAssignableFrom(parameters[i + parameterOffset]))
				{
					throw new ArgumentException($"The type of the method parameter at index {i} is not assignable from the type of the calling parameter.", nameof(parameters));
				}
			}

			string name = string.Empty;

			if (site.DeclaringType != null)
			{
				name += site.DeclaringType.FullName + ".";
			}

			name += "SynergizeAccessor_" + site.Name;

			IEmittableInstruction[] il = new IEmittableInstruction[parameters.Length + 2];

			for (ushort i = 0; i < parameters.Length; ++i)
			{
				il[i] = Instruction.Ldarg(i);
			}

			il[parameters.Length + 0] = site.IsConstructor
				? (IEmittableInstruction) Instruction.Newobj((ConstructorInfo) site)
				: site.IsStatic
					? Instruction.Call((MethodInfo) site)
					: Instruction.Callvirt((MethodInfo) site);

			il[parameters.Length + 1] = Instruction.Ret;

			return new Compiler(returnValue, name, parameters, new EnumerableInjector(il));
		}

		private static Delegate MethodInvocator(Type delegateType, MethodBase site)
		{
			ExtractDelegateInvoke(delegateType, out MethodInfo invoke, out Type[] parameters);

			return MethodInvocatorCompiler(site, invoke.ReturnType, parameters)
				.Compile()
				.CreateDelegate(delegateType);
		}

		public static TDelegate MethodInvocator<TDelegate>(MethodBase site) where TDelegate : Delegate
		{
			return (TDelegate) MethodInvocator(typeof(TDelegate), site);
		}

		public static Delegate MethodInvocator(Type delegateType, Type type, string name)
		{
			MethodInfo site = Method(delegateType, type, name, out MethodInfo invoke, out Type[] parameters);
			return MethodInvocatorCompiler(site, invoke.ReturnType, parameters)
				.Compile()
				.CreateDelegate(delegateType);
		}

		public static TDelegate MethodInvocator<TDelegate>(Type type, string name) where TDelegate : Delegate
		{
			return (TDelegate) MethodInvocator(typeof(TDelegate), type, name);
		}

		public static TDelegate MethodInvocator<TDeclarer, TDelegate>(string name) where TDelegate : Delegate
		{
			return MethodInvocator<TDelegate>(typeof(TDeclarer), name);
		}

		#endregion

		#region Constructors

		public static Delegate ConstructorInvocator(Type delegateType, ConstructorInfo site)
		{
			ExtractDelegateInvoke(delegateType, out MethodInfo invoke, out Type[] parameters);

			return MethodInvocatorCompiler(site, invoke.ReturnType, parameters)
				.Compile()
				.CreateDelegate(delegateType);
		}

		public static TDelegate ConstructorInvocator<TDelegate>(ConstructorInfo site) where TDelegate : Delegate
		{
			return (TDelegate) ConstructorInvocator(typeof(TDelegate), site);
		}

		public static Delegate ConstructorInvocator(Type delegateType, Type type)
		{
			return MethodInvocator(delegateType, Constructor(delegateType, type));
		}

		public static TDelegate ConstructorInvocator<TDelegate>(Type type) where TDelegate : Delegate
		{
			return (TDelegate) ConstructorInvocator(typeof(TDelegate), type);
		}

		public static TDelegate ConstructorInvocator<TDeclarer, TDelegate>(string name) where TDelegate : Delegate
		{
			return MethodInvocator<TDelegate>(typeof(TDeclarer), name);
		}

		#endregion
	}
}
