﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Synergize.Compilers;
using Synergize.Instructions;
using Synergize.Instructions.Injectors;

namespace Synergize
{
	public static partial class AccessTools
	{
		private static readonly Dictionary<Type, Dictionary<MethodInfo, MethodInfo>> Invocators;
		private static readonly Dictionary<MethodInfo, int> InvocatorTokens;
		private static readonly Dictionary<int, MethodInfo> TokenInvocators;
		private static int _tokenCounter;

		static AccessTools()
		{
			_tokenCounter = int.MinValue;

			Invocators = new Dictionary<Type, Dictionary<MethodInfo, MethodInfo>>();
			TokenInvocators = new Dictionary<int, MethodInfo>();
			InvocatorTokens = new Dictionary<MethodInfo, int>();
		}

		private static IEnumerable<IEmittableInstruction> GenerateInvocatorInstructions(MethodInfo target, IReadOnlyList<ParameterInfo> sourceParameters)
		{
			byte offset;
			if (!target.IsStatic)
			{
				yield return Instruction.Ldarg(0);

				offset = 1;
			}
			else
			{
				offset = 0;
			}

			foreach (ParameterInfo targetParameter in target.GetParameters())
			{
				bool found = false;
				for (ushort i = 0; i < sourceParameters.Count; ++i)
				{
					ParameterInfo fullParameter = sourceParameters[i];

					if (fullParameter.Name != targetParameter.Name)
					{
						continue;
					}

					if (!targetParameter.ParameterType.IsAssignableFrom(fullParameter.ParameterType.StripRef(out bool fullByRef)))
					{
						throw new ArgumentException(
							$"Expected type of {fullParameter.ParameterType} or dereferenced, but target was {targetParameter.ParameterType} (name: {fullParameter.Name}).",
							nameof(targetParameter));
					}

					yield return Instruction.Ldarg((ushort) (i + offset));

					if (fullByRef)
					{
						yield return Instruction.Ldind(targetParameter.ParameterType);
					}

					found = true;
					break;
				}

				if (!found)
				{
					throw new ArgumentException("Unknown target parameter encountered: " + targetParameter, nameof(targetParameter));
				}
			}

			yield return target.IsStatic ? Instruction.Call(target) : Instruction.Callvirt(target);
			yield return Instruction.Ret;
		}

		private static MethodInfo GetOrCreateInvocator(Type delegateType, MethodInfo target)
		{
			if (!Invocators.TryGetValue(delegateType, out Dictionary<MethodInfo, MethodInfo> targets))
			{
				targets = new Dictionary<MethodInfo, MethodInfo>();
				Invocators.Add(delegateType, targets);
			}

			if (!targets.TryGetValue(target, out MethodInfo invocator))
			{
				invocator = LossyInvocatorNoCheck(delegateType, target);
				targets.Add(target, invocator);
			}

			if (!InvocatorTokens.TryGetValue(invocator, out int token))
			{
				token = _tokenCounter++;
				InvocatorTokens.Add(invocator, token);
			}

			if (!TokenInvocators.ContainsKey(token))
			{
				TokenInvocators.Add(token, invocator);
			}

			return invocator;
		}

		private static MethodInfo LossyInvocatorNoCheck(Type delegateType, MethodInfo target)
		{
			ExtractDelegateInvoke(delegateType, out MethodInfo source, out Type[] sourceParameters);

			if (!target.IsStatic)
			{
				Type[] newSourceParameters = new Type[sourceParameters.Length + 1];
				newSourceParameters[0] = target.DeclaringType;
				Array.Copy(sourceParameters, 0, newSourceParameters, 1, sourceParameters.Length);

				sourceParameters = newSourceParameters;
			}

			return new Compiler(source.ReturnType,
				target.ToTypeString() + "[SynergizedLossy]",
				sourceParameters,
				new EnumerableInjector(GenerateInvocatorInstructions(target, source.GetParameters()))).Compile();
		}

		public static Delegate LossyInvocator(Type delegateType, MethodInfo target)
		{
			if (!typeof(Delegate).IsAssignableFrom(delegateType))
			{
				throw new ArgumentException("The delegate type must be a subclass of " + nameof(Delegate), nameof(delegateType));
			}

			if (!target.IsStatic)
			{
				throw new ArgumentException("This method is only for static targets. For non-static targets, an invocator instantiator must be made.", nameof(target));
			}

			return GetOrCreateInvocator(delegateType, target).CreateDelegate(delegateType);
		}

		public static TDelegate LossyInvocator<TDelegate>(MethodInfo target) where TDelegate : Delegate
		{
			return (TDelegate) LossyInvocator(typeof(TDelegate), target);
		}

		public static Func<object, Delegate> LossyInvocatorInstantiator(Type delegateType, MethodInfo target)
		{
			if (!typeof(Delegate).IsAssignableFrom(delegateType))
			{
				throw new ArgumentException("The delegate type must be a subclass of " + nameof(Delegate), nameof(delegateType));
			}

			if (target.IsStatic)
			{
				throw new ArgumentException("This method is only for non-static targets. For static targets, an direct invocator must be made.", nameof(target));
			}

			MethodInfo invocator = GetOrCreateInvocator(delegateType, target);

			return (Func<object, Delegate>) new Compiler(delegateType,
				target.Name + "[Instantiator]",
				new[]
				{
					typeof(object)
				},
				new EnumerableInjector(Instruction.Ldsfld(Field(typeof(AccessTools), nameof(TokenInvocators))),
					Instruction.Ldc_I4(InvocatorTokens[invocator]),
					Instruction.Callvirt(Property(TokenInvocators.GetType(), "Item").GetMethod),
					Instruction.Ldtoken(delegateType),
					Instruction.Call(Method<Type>(nameof(System.Type.GetTypeFromHandle))),
					Instruction.Ldarg(0),
					Instruction.Callvirt(Method<MethodInfo>(nameof(MethodInfo.CreateDelegate), typeof(Type), typeof(object))),
					Instruction.Castclass(delegateType),
					Instruction.Ret)).Compile().CreateDelegate(typeof(Func<,>).MakeGenericType(typeof(object), delegateType));
		}

		public static Func<object, TDelegate> LossyInvocatorInstantiator<TDelegate>(MethodInfo target) where TDelegate : Delegate
		{
			return (Func<object, TDelegate>) LossyInvocatorInstantiator(typeof(TDelegate), target);
		}
	}
}
