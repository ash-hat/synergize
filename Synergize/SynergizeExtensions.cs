﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;

using Synergize.Compilers;
using Synergize.Instructions;
using Synergize.Instructions.Injectors;

namespace Synergize
{
	public static class SynergizeExtensions
	{
		private const string DynamicHandleGetterExceptionMessage = "Current runtime contains a " + nameof(DynamicMethod) + " handle retrieval process that is not supported.";
		private const string DynamicPreparerExceptionMessage = "Current runtime contains a " + nameof(DynamicMethod) + " compilation process that is not supported.";

		private static readonly Func<DynamicMethod, RuntimeMethodHandle> DynamicHandleGetter;
		private static readonly Action<DynamicMethod> DynamicPreparer;

		static SynergizeExtensions()
		{
			DynamicHandleGetter = ReflectiveDynamicHandleGetter;
			DynamicPreparer = ReflectiveDynamicPreparer;

			DynamicHandleGetter = CreateDynamicHandleGetter();
			DynamicPreparer = CreateDynamicPreparer();
		}

		private static Func<DynamicMethod, RuntimeMethodHandle> CreateDynamicHandleGetter()
		{
			MethodInfo dynamicHandleMethod = AccessTools.Method<DynamicMethod>("GetMethodDescriptor");
			if (dynamicHandleMethod != null)
			{
				return AccessTools.MethodInvocator<Func<DynamicMethod, RuntimeMethodHandle>>(dynamicHandleMethod);
			}

			// Mono
			FieldInfo dynamicHandleField = AccessTools.Field<DynamicMethod>("m_method") ?? AccessTools.Field<DynamicMethod>("mhandle");
			if (dynamicHandleField != null)
			{
				return AccessTools.FieldGetterInvocator<DynamicMethod, RuntimeMethodHandle>(dynamicHandleField);
			}

			throw new NotSupportedException("Current runtime contains a " + nameof(DynamicMethod) + " type not supported.");
		}

		private static Action<DynamicMethod> CreateDynamicPreparer()
		{
			// Mono
			MethodInfo directMethod = AccessTools.Method<DynamicMethod>("CreateDynMethod");
			if (directMethod != null)
			{
				return AccessTools.MethodInvocator<Action<DynamicMethod>>(directMethod);
			}

			MethodInfo runtimeCompiler = AccessTools.Method(typeof(RuntimeHelpers), "_CompileMethod");
			if (runtimeCompiler != null)
			{
				ParameterInfo[] parameters = runtimeCompiler.GetParameters();

				if (parameters.Length == 1)
				{
					Type parameterType = parameters[0].ParameterType;

					IInjector compileFromHandle;

					if (parameterType == typeof(RuntimeMethodHandle))
					{
						compileFromHandle = new EnumerableInjector(Instruction.Call(DynamicHandleGetter.Method),
							Instruction.Call(runtimeCompiler),
							Instruction.Ret);
					}
					else if (parameterType == typeof(IntPtr))
					{
						compileFromHandle = new EnumerableInjector(Instruction.Call(AccessTools.Property<RuntimeMethodHandle>(nameof(RuntimeMethodHandle.Value)).GetMethod),
							Instruction.Call(runtimeCompiler),
							Instruction.Ret);
					}
					else
					{
						Type runtimeMethodInfo = Type.GetType("System.IRuntimeMethodInfo");
						if (runtimeMethodInfo != null && parameterType == runtimeMethodInfo)
						{
							compileFromHandle = new EnumerableInjector(Instruction.Callvirt(AccessTools.Method<RuntimeMethodHandle>("GetMethodInfo")),
								Instruction.Call(runtimeCompiler),
								Instruction.Ret);
						}
						else
						{
							throw new NotSupportedException(DynamicPreparerExceptionMessage);
						}
					}

					return new Compiler(typeof(void),
						"DynamicPreparer",
						new[]
						{
							typeof(DynamicMethod)
						},
						new EnumerableInjector(Instruction.Ldarg(0),
								Instruction.Ldsfld(AccessTools.Field(typeof(SynergizeExtensions), nameof(DynamicHandleGetter))),
								Instruction.Callvirt(AccessTools.Method<Func<DynamicMethod, RuntimeMethodHandle>>("Invoke"))).Merge(compileFromHandle)
							.Merge(new EnumerableInjector(Instruction.Call(runtimeCompiler), Instruction.Ret))).Compile().CreateDelegate<Action<DynamicMethod>>();
				}
			}

			throw new NotSupportedException(DynamicPreparerExceptionMessage);
		}

		private static RuntimeMethodHandle ReflectiveDynamicHandleGetter(DynamicMethod method)
		{
			MethodInfo getter = AccessTools.Method<DynamicMethod>("GetMethodDescriptor");
			if (getter != null)
			{
				return (RuntimeMethodHandle) getter.Invoke(method, new object[0]);
			}

			// .Net Core ?? Mono
			FieldInfo field = AccessTools.Field<DynamicMethod>("m_method") ?? AccessTools.Field<DynamicMethod>("mhandle");
			if (field != null)
			{
				return (RuntimeMethodHandle) field.GetValue(method);
			}

			throw new NotImplementedException(DynamicHandleGetterExceptionMessage);
		}

		private static void ReflectiveDynamicPreparer(DynamicMethod method)
		{
			// Mono
			MethodInfo createDynMethod = AccessTools.Method<DynamicMethod>("CreateDynMethod");
			if (createDynMethod != null)
			{
				createDynMethod.Invoke(method, new object[0]);
				return;
			}

			MethodInfo compileMethod = AccessTools.Method(typeof(RuntimeHelpers), "_CompileMethod");

			MethodInfo getMethodDescriptor = AccessTools.Method<DynamicMethod>("GetMethodDescriptor");
			RuntimeMethodHandle handle = (RuntimeMethodHandle) getMethodDescriptor.Invoke(method, new object[0]);

			MethodInfo getMethodInfo = AccessTools.Method<RuntimeMethodHandle>("GetMethodInfo");
			if (getMethodInfo != null)
			{
				try
				{
					compileMethod.Invoke(null, new[]
					{
						getMethodInfo.Invoke(handle, new object[0])
					});

					return;
				}
				catch (TargetInvocationException e)
				{
					if (e.InnerException is InvalidProgramException)
					{
						throw;
					}

					// ignored
				}
			}

			if (compileMethod.GetParameters()[0].ParameterType.IsInstanceOfType(handle.Value))
			{
				compileMethod.Invoke(null, new object[]
				{
					handle.Value
				});

				return;
			}

			if (compileMethod.GetParameters()[0].ParameterType.IsInstanceOfType(handle))
			{
				compileMethod.Invoke(null, new object[]
				{
					handle
				});

				return;
			}

			throw new NotSupportedException(DynamicPreparerExceptionMessage);
		}

		public static TItem[] ArrayFlatten<TItem, TSubList>(this IReadOnlyList<TSubList> jaggedList) where TSubList : IReadOnlyList<TItem>
		{
			int count = 0;
			foreach (TSubList subList in jaggedList)
			{
				count += subList.Count;
			}

			TItem[] flattened = new TItem[count];
			count = 0;

			foreach (TSubList subList in jaggedList)
			{
				for (int j = 0; j < subList.Count; ++j)
				{
					flattened[count + j] = subList[j];
				}

				count += subList.Count;
			}

			return flattened;
		}

		public static TResult[] ArraySelect<TItem, TResult>(this IReadOnlyList<TItem> items, Func<TItem, int, TResult> predicate)
		{
			TResult[] results = new TResult[items.Count];

			for (int i = 0; i < results.Length; ++i)
			{
				results[i] = predicate(items[i], i);
			}

			return results;
		}

		public static TDelegate CreateDelegate<TDelegate>(this MethodInfo method) where TDelegate : Delegate
		{
			return (TDelegate) method.CreateDelegate(typeof(TDelegate));
		}

		public static IEnumerable<TItem> Duplicate<TItem>(this IEnumerable<TItem> items)
		{
			foreach (TItem item in items)
			{
				yield return item;
			}
		}

		public static Type GetReturnType(this MethodBase method) =>
			method is ConstructorInfo
				? method.DeclaringType
				: ((MethodInfo) method).ReturnType;

		public static RuntimeMethodHandle GetRuntimeMethodHandle(this MethodBase method)
		{
			DynamicMethod dynamicMethod = method as DynamicMethod;
			return dynamicMethod == null ? method.MethodHandle : ReflectiveDynamicHandleGetter(dynamicMethod);
		}

		public static int GetStackDelta(this OpCode opCode)
		{
			int delta;

			switch (opCode.StackBehaviourPush)
			{
				case StackBehaviour.Push0:
					delta = 0;
					break;

				case StackBehaviour.Push1:
					delta = 1;
					break;

				case StackBehaviour.Push1_push1:
					delta = 2;
					break;

				case StackBehaviour.Varpush:
					delta = 1;
					break;

				case StackBehaviour.Pushi:
					delta = 1;
					break;

				case StackBehaviour.Pushi8:
					delta = 1;
					break;

				case StackBehaviour.Pushr4:
					delta = 1;
					break;

				case StackBehaviour.Pushr8:
					delta = 1;
					break;

				case StackBehaviour.Pushref:
					delta = 1;
					break;

				default: throw new NotSupportedException("Push behavior out of range.");
			}

			switch (opCode.StackBehaviourPop)
			{
				case StackBehaviour.Pop0:
					delta -= 0;
					break;

				case StackBehaviour.Pop1:
					delta -= 1;
					break;

				case StackBehaviour.Pop1_pop1:
					delta -= 2;
					break;

				case StackBehaviour.Varpop:
					delta -= 1;
					break;

				case StackBehaviour.Popi:
					delta -= 1;
					break;

				case StackBehaviour.Popi_pop1:
					delta -= 2;
					break;

				case StackBehaviour.Popi_popi:
					delta -= 2;
					break;

				case StackBehaviour.Popi_popi8:
					delta -= 2;
					break;

				case StackBehaviour.Popi_popr4:
					delta -= 2;
					break;

				case StackBehaviour.Popi_popr8:
					delta -= 2;
					break;

				case StackBehaviour.Popi_popi_popi:
					delta -= 3;
					break;

				case StackBehaviour.Popref:
					delta -= 1;
					break;

				case StackBehaviour.Popref_popi:
					delta -= 2;
					break;

				case StackBehaviour.Popref_pop1:
					delta -= 2;
					break;

				case StackBehaviour.Popref_popi_pop1:
					delta -= 3;
					break;

				case StackBehaviour.Popref_popi_popi:
					delta -= 3;
					break;

				case StackBehaviour.Popref_popi_popi8:
					delta -= 3;
					break;

				case StackBehaviour.Popref_popi_popr4:
					delta -= 3;
					break;

				case StackBehaviour.Popref_popi_popr8:
					delta -= 3;
					break;

				case StackBehaviour.Popref_popi_popref:
					delta -= 3;
					break;

				default: throw new NotSupportedException("Pop behavior out of range.");
			}

			return delta;
		}

		public static int IndexOf<TItem>(this IEnumerable<TItem> items, Func<TItem, bool> predicate)
		{
			int i = 0;

			foreach (TItem item in items)
			{
				if (predicate(item))
				{
					return i;
				}

				i++;
			}

			return -1;
		}

		public static bool IsClass(this Type type)
		{
			return !type.IsValueType;
		}

		public static bool IsStruct(this Type type)
		{
			return type.IsValueType && !IsValue(type) && !IsVoid(type);
		}

		public static bool IsValue(this Type type)
		{
			return type.IsPrimitive || type.IsEnum;
		}

		public static bool IsVoid(this Type type)
		{
			return type == typeof(void);
		}

		public static IDisposable MergeDispose(this IDisposable original, IDisposable concatenated)
		{
			return new MergedDisposable(original, concatenated);
		}

		public static void Prepare(this MethodBase method)
		{
			DynamicMethod dynamicMethod = method as DynamicMethod;
			if (dynamicMethod != null)
			{
				DynamicPreparer(dynamicMethod);
			}
			else
			{
				RuntimeHelpers.PrepareMethod(method.GetRuntimeMethodHandle());
			}
		}

		public static Type StripRef(this Type type, out bool byRef)
		{
			byRef = type.IsByRef;
			return byRef
				? type.GetElementType()
				: type;
		}

		public static string ToFullString(this MethodBase method)
		{
			StringBuilder builder = new StringBuilder();

			builder.Append(method.GetReturnType());
			builder.Append(' ');

			if (method.DeclaringType != null)
			{
				builder.Append(method.DeclaringType);
				builder.Append('.');
			}

			builder.Append(method.Name);
			builder.Append('(');

			ParameterInfo[] parameters = method.GetParameters();
			for (int i = 0; i < parameters.Length; ++i)
			{
				ParameterInfo parameter = parameters[i];

				builder.Append(parameter.ParameterType);
				builder.Append(' ');
				builder.Append(parameter.Name);
				if (i + 1 < parameters.Length)
				{
					builder.Append(", ");
				}
			}

			builder.Append(')');

			return builder.ToString();
		}

		public static string ToTypeString(this MethodBase method)
		{
			string str = method.DeclaringType != null ? method.DeclaringType + "." : string.Empty;

			str += method.Name;

			return str;
		}
	}
}
