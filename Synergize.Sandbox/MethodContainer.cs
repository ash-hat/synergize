﻿using System;
using Synergize.Instructions.Injectors;

namespace Synergize.Sandbox
{
	public class MethodContainer
	{
		private static int accessToolsTestField;

		private static string AccessToolsTestProperty => "hi";
		
		public string OriginalInstance(int a)
		{
			Console.WriteLine(GetType() + " " + a);
			return "Instance call";
		}
		
		public static void OriginalInstanceReplacement([ImportInstance] MethodContainer instance, [ImportArgument] int a)
		{
			Console.WriteLine("REPLACED: " + instance.GetType() + " " + a);
		}
		
		public static string Original(int a)
		{
			Console.WriteLine("A");
			
			try
			{
				throw new Exception();
				
				switch (a)
				{
					case 3:
						return "This cool value is: " + a;
					case 4:
						return "Not 3.";
					default:
						return "Not 3 or 4.";
				}
			}
			catch
			{
				return "THREW";
			}
		}

		public static string OriginalReplacement(int a)
		{
			Console.WriteLine("B");

			return "Replaced";
		}

		public static void Postfix(ref string result)
		{
			result = "ORIGINAL: " + result;
		}

		public static HandledReturn<string> AttributePostfix([ImportArgument] int a)
		{
			return null;
		}

		private static bool AccessToolsTestMethod(byte a)
		{
			return a == 1;
		}

		private static bool StaticLossyTestMethod(int c, byte a)
		{
			return c == a;
		}
		
		private bool LossyTestMethod(int c, byte a)
		{
			return GetType() == typeof(MethodContainer) && StaticLossyTestMethod(c, a);
		}
	}
}